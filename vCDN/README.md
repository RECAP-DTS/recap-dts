# RECAP vCDN Simulator

The RECAP vCDN Simulator is a generalized and extensible simulation framework that enables the seamless simulation and experimentation of virtual Content Distribution Networks (vCDNs). The framework is written using the C/C++ programming language and OpenMP API and enables the exploitation of distributed and shared memory parallel techniques for the acceleration of Cloud simulation.

The project proposes to provide a valuable validation solution to vCDN providers, by simulating the provided infrastructure and several virtual cache placements to be tested.

Our goal is to address optimal virtual cache placement in the cloud after thorough experimentation, regarding both providers' and consumers' benefits. From the provider's perspective, efficient resource consumption and provisioning, regarding computational, memory and storage resources are the most essential. Additionally, network congestion minimisation is considered mandatory. From the consumer's perspective, the simulator addresses the most sought-after end-user latency reduction, with the use of load balancing algorithms and applying request servicing closer to the end user. 

The architecture of the RECAP vCDN Simulator is presented in the following articles, please cite them if you use the simulator:

* Christos K. Filelis-Papadopoulos, Konstantinos M. Giannoutakis, George A. Gravvanis, Patricia Takako Endo, Dimitrios Tzovaras, Sergej Svorobej, Theo Lynn: Simulating large vCDN networks: A parallel approach, Simulation Modelling Practice and Theory, Volume 92, 2019, Pages 100-114, ISSN 1569-190X, https://doi.org/10.1016/j.simpat.2019.01.001

* Christos K. Filelis-Papadopoulos, Patricia Takako Endo, Malika Bendechache, Sergej Svorobej, Konstantinos M. Giannoutakis, George A. Gravvanis, Dimitrios Tzovaras, James Byrne, Theo Lynn: Towards simulation and optimization of cache placement on large virtual content distribution networks, Journal of Computational Science, Volume 39, 2020, 101052, ISSN 1877-7503, https://doi.org/10.1016/j.jocs.2019.101052

For feedback, troubleshooting and technical inquiries, please contact mspanopoulos@iti.gr or kgiannou@iti.gr

## Getting started

Clone the project:

     git clone https://RECAP-DTS@bitbucket.org/RECAP-DTS/recap-dts.git

## Prerequisites

* Required g++ version 4.7 or higher

## Input and Output Data

Input files are located under the corresponding *input* folder of the project. Five input files should be provided to the simulator, with specific order. The first one, *sim.txt*, contains all the simulation parameters, such as start-end simulation time, update interval, request rate, passthrough (lets requests flow upwards), pathstrategy (inlcude cache searching), search (static or dynamic) and threads. The second one, *G.mtx*, is the graph provided from the matrix market, describing and containing the connections between the nodes and the corresponding bandwidth value. The third file, *G.txt*, denotes the information of all sites (ID, vCPUs, Memory, Storage, etc.). The fourth file, *content.txt*, contains the requirements for each type of content. Fifth file, *vm.txt*, denotes cache deployment on any provided site (ID), of specific content, total number of resources and multipliers. Note that each file can be adjusted by the user before execution.

Output files are of three types. The first file, *out.txt*, denotes total execution time and total number of requests. The two other types are grouped files under two corresponding folders, namely *net* and *sites*. Each file is exported on each respective update interval. *Net* contains the link metrics, such as bandwidth consumption, while *sites* contain metrics of each site, such as cumulative resource consumption and accepted/rejected requests. Output files extraction path is given from the user.

## Project Build

Navigate to the directory of the project, open the terminal and enter

     $ make clean && make

## Run the RECAP vCDN Simulator

At the directory of the project, open the terminal and enter

     $ cd bin

where the executable is located, and then

     $ ./recap_sim ../input/sim.txt ../input/G.mtx ../input/G.txt ../input/content.txt ../input/vm.txt <desired output path> (e.g. ../output/)

After running the simulation, the corresponding output files, *out.txt*, *net* and *sites* folders, are exported to the provided output path.

## Deliverables

The deliverables of the RECAP Project can be found at the following url:

     https://recap-project.eu/about/public-deliverables/

## Acknowledgements

RECAP was funded by the European Commission’s Horizon 2020 Programme for Research and Innovation under Grand Agreement No. 732667.
