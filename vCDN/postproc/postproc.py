#!/usr/bin/python

import sys
import functools
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
import pandas as pd

print("Loading Simulation Results...")

timestep = 200;

f = open('../output/net/out_net_'+str(timestep),'r')
data = [tuple(float(n) for n in line.split()) for line in f]
data = np.array(data)

G=nx.Graph()
G.add_edges_from(zip(data[1:15,0],data[1:15,1]))



#g_data=G=nx.from_pandas_edgelist(df, 0, 1, edge_attr=True)

nx.draw(G, with_labels=True, node_size=100, node_color="skyblue", pos=nx.random_layout(G))
plt.title("fruchterman_reingold")
plt.show()






