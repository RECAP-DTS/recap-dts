/* Copyright 2019 The RECAP Simulation Framework Authors. All
Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#ifndef SIM_H
#define SIM_H
#include<site.h>
#include<ngraph.h>
#include<content.h>
#include<vector>
using namespace std;

class sim {
    private:
        int alloc;
        double *Time;
	double *minmaxReqPerSec;
	int passthrough;
	int pathstrategy;
	int search;
	int threads;
	int numOfVMs;
	int distSize;
        vector<site> *S;
        graph *G;
	content *C;

	// Out_Dir
	string out_dir;

	// Performance (seconds)
	double elapsedTime;

	// Map for forming deployment paths
	int ldg,last;
	int *map;

	// All Requests
	int allReq;

	// Inverse Mapping for output
	int *invMap;

	// Map for workload distribution
	int **dist;

    public:
        sim();
	sim(const char *simprop,const char* graphprop,const char* siteprop,const char* contprop,const char *vmpos,const char* out_dir_name);
        sim(const sim & t);
        sim & operator=(const sim & t);
        ~sim();
	void loadICforVMs(const char *vmpos);
        int isalloc() const;
	void run();

        double *gTime() const;

	void createDeployRequestList();


	void addUser(int Site,int Content,double Duration,double vCPU,double Memory,double Storage,double Network);
	void formDeploymentPath(int sSite,int Content,double Duration,double vCPU,double Memory,double Storage,double Network,list<vector<int>> *svr);
	double *gminmaxReqPerSec() const;
	vector<site> *gS() const;
        graph *gG() const;
	content *gC() const;
	int getAllRequests();
	void timestep(double tstep,double amount=1.0);
	void toFile(double tstep);


	void placements(double *pl);
};

#endif
