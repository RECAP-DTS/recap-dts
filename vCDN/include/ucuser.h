/* Copyright 2019 The RECAP Simulation Framework Authors. All
Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#ifndef UCUSER_H
#define UCUSER_H

class ucuser {
private:
	int alloc;
	double netload;
	double duration;
	int endpoint;
public:
	ucuser();
	ucuser(double L_netload, double L_duration,int L_endpoint);
	ucuser(const ucuser &t);
	ucuser & operator=(const ucuser & t);
	~ucuser();

	double reduceDuration(double amount);
	int isalloc() const;
	double gnetload() const;
	double gduration() const;
	int gendpoint() const;
};

#endif
