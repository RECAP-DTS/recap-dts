/* Copyright 2019 The RECAP Simulation Framework Authors. All
Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#ifndef NODE_H
#define NODE_H
#include <power.h>

class node {
private:
	int alloc; //Allocation of class
	double *vCPU; //Required vCPUs
	double *Memory; //Required Memory
	double *Storage; //Required Storage
	int numOfVMs;
	int ActiveLinks;
	int active;
	power *P;

	double availability;
	int meanTimeMin,meanTimeMax;
	int up; // (up = 0 means the server is up)

public:
	node();
	node(double L_vCPU,double L_Memory,double L_Storage,int L_ModelType,int L_nP,double *L_P,double L_availability,double L_meanTimeMin,double L_meanTimeMax);
	node(const node & t);
	node & operator=(const node & t);
	~node();

	int isalloc() const;
	int checkAvailability(double L_vCPU,double L_Memory,double L_Storage);
	void addVM(double L_vCPU,double L_Memory,double L_Storage);
	void removeVM(double L_vCPU,double L_Memory,double L_Storage);
	void addUser(double L_vCPU,double L_Memory,double L_Storage,int nUsers);
	void addUser(double L_vCPU,double L_Memory,double L_Storage);
	void removeUser(double num,double L_vCPU,double L_Memory,double L_Storage);
	double *gvCPU() const;
	double *gMemory() const;
	double *gStorage() const;
	int gnumOfVMs() const;
	int gActiveLinks() const;
	int gactive() const;
	int gup() const;
	double getPowerConsumption();
	void checkFailure();

};


#endif
