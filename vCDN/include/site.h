/* Copyright 2019 The RECAP Simulation Framework Authors. All
Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#ifndef SITE_H
#define SITE_H
#include <vm.h>
#include <node.h>
#include <ucuser.h>
#include <power.h>
#include <list>
#include <map>
#include <vector>

class site {
private:
	int alloc; //Allocation of class
	int ID; //ID of the site
	int Type; //Type of site (1,2,3,...)==>(Core Site, Metro Site, MAN,...)
	double *UL; //Utilized UL bandwidth [uUL,tUL] Gbps (SITE)
	int *Cache_hm; //Cache hits and misses (SITE)
	int *NumOfUDLinks; //Number of Upload/Download links for a site (SITE)
	int **ID_UDLinks; //IDs of UL/DL sites (SITE)
	int *UsersAR; //Accepted and Rejected requests for users (SITE)
	double **ID_UDBWs; //Bandwidths of UL/DL links (SITE)
	double pCons; //Power Consumption for the installed hardware (SITE)
	double Latency; //Latency is measured only on last level sites
	int minReqPerTstep; // Minimum number of requests per time step
	int maxReqPerTstep; // Maximum number of requests per time step
	int premises; // Total number of supported requests
	int prevUsers; // Only for dynamic search

	std::map<int,int> *CachedContent; //Map of Cached content (SITE)
	std::list<vm> *VMs; //List of active VMs (SITE)
	std::vector<node> *Nodes; //List of nodes on a (SITE)
	std::list<ucuser> *ucUsers; //List of uncached users per (SITE)
public:
	site();
	site(int L_ID,int L_Type,double L_vCPU,double L_Memory,double L_Storage,int L_NumOfUL,int L_NumOfDL,int *L_IDOfUL,int *L_IDOfDL,double *L_BWOfUL,double *L_BWOfDL);
	void addnode(double L_vCPU,double L_Memory,double L_Storage,int L_ModelType,int L_nP,double *L_P,double L_availability, double L_meanTimeMin,double L_meanTimeMax);
	site(const site & t);
	site & operator=(const site & t);
	~site();

	int numOfVMs() const;
	void enqueVM(const vm & t);
	void cleanup();
	int VMallocStrategy(vm & t);
	int checkAvailability(int Content,double Duration,double L_vCPU,double L_Memory,double L_Storage,double L_Network,int endPoint,int *vmn);
	double getLinkSpeed(int SiteID,int UpDown);
	int getLocalLinkID(int SiteID,int UpDown);

	int isalloc() const;
	int isCached(int L_Type) const;
	int NumOfNodes() const;
	int gID() const;
	int gType() const;
	double *gUL() const;
	int *gCache_hm() const;
	int *gNumOfUDLinks() const;
	int **gID_UDLinks() const;
	double **gID_UDBWs() const;
	int *gUsersAR() const;
	std::map<int,int> *gCachedContent() const;
	std::list<vm> *gVMs() const;
	std::vector<node> *gNodes() const;
	std::list<ucuser> *gucUsers() const;
	int gNodeType() const;
	double gpCons() const;
	double gLatency() const;
	int getMinReq() const;
	int getMaxReq() const;
	int getPrevUsers();
	void setPrevUsers(int users);
	void addUserUncached(double Netload,double Duration,int EndPoint);
	void addUserToVM(double Duration,double L_vCPU,double L_Memory,double L_Storage,double L_Network,int DeplType,int inc,int EndPoint);
	void collect_stats(int *active_links,int *active_nodes,double *UTvCPU, double *UTMemory,double *UTStorage);
	void timestep(double amount=1.0);
	void incLatUponReq(double hops, double netload, int endpoint);
	void setMinReq(int minreq);
	void setMaxReq(int maxreq);

	//Printers for debugging
	void printInfo();
};


#endif
