/* Copyright 2019 The RECAP Simulation Framework Authors. All
Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#ifndef NGRAPH_H
#define NGRAPH_H

class graph {
private:
	int alloc; //Allocation of class
	int rows,cols; //Number of rows and cols of the graph (usually rows=cols)
	int nnz; //Nonzeros of the CSR graph
	int nnzt; //Nonzeros of the its CSC counterpart
	int *ix; //Row counts (CSR)
	int *jx; //Column indexes (CSR)
	double *x; //Bandwidths (Gbps) (CSR)
	double *d; //Diagonal elements
	double **Spec; // vCPUs / Memory / Storage in (Number/GB/GB) (First level does not have download links)
	int *MaxNodes; //Max Nodes per site
	int **TypStr; //Types and Strategies per site
	int *OffsetLevel; //Numerical offset to retrieve all sites of type i
	int *nP; // Number of points for the power model
	double **P; // Power measurements
	int AllNodes; // AllNodes;
	int *sid; // Site IDs per node
public:
	graph();
	graph(const char* name);
	graph(const char* name,const char* SpecsTypes_filename);
	graph(const graph &t);
	int isalloc() const;
	int getNumOfSites() const;
	int getNumOfSites(int id) const;
	int getConnections() const;
	void getULlinks(int id,int **ids,double **speeds,int *nids);
	void getDLlinks(int id,int **ids,double **speeds,int *nids);
	void getNodes(int id, int **ids,int *nids);
	int getLevel(int id);
	int getMaxLevel() const;
	int getOffsetLevel(int Type) const;
	int getMaxType();
	void parseSpecsTypes(const char* name);
	double getvCPU(int id);
	double getMemory(int id);
	double getStorage(int id);
	double getAvailability(int id);
	double getMeanTime(int id,int minmax);
	int getMaxNodes(int id);
	int getModelType(int id);
	int getSID(int id);
	void sanityCheck();
	int getAllNodes();
	int getNumOfPPoints(int id);
	double *getPPoints(int id);
	graph & operator=(const graph & t);

	//Low level access
	int *gix() const;
	int *gjx() const;
	double *gx() const;
	double *gd() const;
	double **gSpec() const;
	int *gMaxNodes() const;
	int **gTypStr() const;

	~graph();
};





#endif
