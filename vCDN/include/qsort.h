/* Copyright 2019 The RECAP Simulation Framework Authors. All
Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#ifndef QSORT_H
#define QSORT_H
#include <cmath>
#include <algorithm>
using namespace std;

//Qsort(const T, int, int) - Quicksort of integers from left to right. 
//Let arr be [0 .. n-1] then left=0 and right=n-1, ascdes=0 (ascending)
//ascdes=1 (descending)
template <class T> void Qsort(T * arr, const int & left,const int & right, const int & ascdes)
{
	int i = left, j = right;
	T tmp;
	T pivot = arr[(left + right) / 2];
	while (i <= j) 
	{
            if (ascdes==0)
            {
				while (arr[i] < pivot)
					i++;
                while (arr[j] > pivot)
                    j--;
            }
            else
            {
           		while (arr[i] > pivot)
					i++;
				while (arr[j] < pivot)
					j--;

            }
            if (i <= j) 
            {
				tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;
                i++;
                j--;
            }
	};
	if (left < j)
		Qsort(arr, left, j,ascdes);
	if (i < right)
        Qsort(arr, i, right,ascdes);
}


//Qsort - Qsort R according to T (see above template for definitions)
//arr and arr2 will be sorted according to the length of arr (left,right))
template <class T,class R> void Qsort(T * arr, R * arr2, const int & left,const int & right, const int & ascdes)
{
	int i = left, j = right;
	T tmp;
    R tmp2;
	T pivot = arr[(left + right) / 2];
	while (i <= j) 
	{
            if (ascdes==0)
            {
		while (arr[i] < pivot)
			i++;
                while (arr[j] > pivot)
                        j--;
            }
            else
            {
           	while (arr[i] > pivot)
			i++;
                while (arr[j] < pivot)
                        j--;

            }
            if (i <= j) 
            {
		tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;
                tmp2=arr2[i];
                arr2[i]=arr2[j];
                arr2[j]=tmp2;
                i++;
                j--;
            }
	};
	if (left < j)
		Qsort(arr,arr2, left, j,ascdes);
	if (i < right)
                Qsort(arr,arr2, i, right,ascdes);
}

#endif
