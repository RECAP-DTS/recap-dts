/* Copyright 2019 The RECAP Simulation Framework Authors. All
Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#ifndef USER_H
#define USER_H

class user {
private:
	int alloc;
	double *DCMSN;
	int Endpoint;
	int numOfTries;
public:
	user();
	user(double L_Duration,double L_vCPU,double L_Memory,double L_Storage,double L_Network,double L_Endpoint);
	user(double L_Duration,double L_vCPU,double L_Memory,double L_Storage,double L_Network,double L_Endpoint,int L_numOfTries);	
	user(const user &t);
	user & operator=(const user & t);
	~user();

	double modifyDuration(double amount);
	int isalloc() const;
	double *gDCMSN() const;
	int gEndpoint() const;
	int gnumOfTries() const;
};

#endif
