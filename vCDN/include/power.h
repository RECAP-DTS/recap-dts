/* Copyright 2019 The RECAP Simulation Framework Authors. All
Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#ifndef POWER_H
#define POWER_H

class power {
    private:
        int alloc;
	int ModelType; //Model Type
	int nP; //Number of points in the model
	double *P; //Retains values of Power in Watts based on the model

    public:
        power();
	power(int L_ModelType,int L_nP,double *L_P);
        power(const power & t);
        power & operator=(const power & t);
        ~power();
        int isalloc() const;
	int gModelType() const;
	int gnP() const;
	double *gP() const;
	double getPowerConsumption(double u);
	//For Debugging
	void printInfo();
};

#endif
