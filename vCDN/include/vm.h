/* Copyright 2019 The RECAP Simulation Framework Authors. All
Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#ifndef VM_H
#define VM_H
#include<list>
#include<user.h>

class vm {
private:
	int alloc; //Allocation of class
	int Type; //Type of content [1...n]
	double *vCPU; //Required vCPUs
	double *Memory; //Required Memory
	double *Storage; //Required Storage
	int Node; //ID of Node (1...N) where VM is hosted
	std::list<user> *Users; //Duration per User
	double chForCacheHit;
	double *mults;

public:
	vm();
	vm(int L_Type,double L_vCPU,double L_Memory,double L_Storage,double L_chForCacheHit,double *L_mults);
	vm(const vm & t);
	vm & operator=(const vm & t);
	~vm();

	int isalloc() const;
	void assignNode(int L_Node);
	int checkAvailability(double L_vCPU,double L_Memory,double L_Storage);
	int CurNumOfUsers() const;
	void addUser(double L_Duration,double L_vCPU,double L_Memory,double L_Storage,double L_Network,int L_Endpoint);
	void modifyDuration(int User,double amount);
	int gType() const;
	double *gvCPU() const;
	double *gMemory() const;
	double *gStorage() const;
	int gNode() const;
	double gchForCacheHit() const;
	double *gmults() const;
	std::list<user> *gUsers() const;
	int timestep(double *reqAmounts,double *nl,double amount=1.0);
	int timestep(double *reqAmounts,double amount=1.0);
	
};


#endif
