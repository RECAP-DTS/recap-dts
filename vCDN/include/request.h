/* Copyright 2019 The RECAP Simulation Framework Authors. All
Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#ifndef REQUEST_H
#define REQUEST_H

class request {
private:
	int alloc;
	int content;
	int site;
	double duration;
	double vCPU;
	double memory;
	double storage;
	double network;
public:
	request();
	request(int L_content, int L_site, double L_duration, double L_vCPU, double L_memory, double L_storage, double L_network);
	request(const request &t);
	request & operator=(const request & t);
	~request();

	int isalloc() const;
	int gcontent() const;
	int gsite() const;
	double gduration() const;
	double gvCPU() const;
	double gmemory() const;
	double gstorage() const;
	double gnetwork() const;
};

#endif
