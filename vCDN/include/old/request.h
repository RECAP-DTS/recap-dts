#ifndef REQUEST_H
#define REQUEST_H

class request {
private:
	int alloc;
	int content;
	double duration;
	double Lat;
	double Lon;
	double vCPUs;
	double Memory;
	double Storage;
	double Bandwidth;
public:
	request();
	request(int L_content, double L_duration, double L_Lat, double L_Lon, double L_vCPUs, double L_Memory, double L_Storage, double L_Bandwidth);
	request(const request &t);
	request & operator=(const request & t);
	~request();

	int isalloc() const;
	int gcontent() const;
	double gduration() const;
	double gLat() const;
	double gLon() const;
	double gvCPUs() const;
	double gMemory() const;
	double gStorage() const;
	double gBandwidth() const;
};

#endif
