/* Copyright 2019 The RECAP Simulation Framework Authors. All
Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#ifndef CONTENT_H
#define CONTENT_H

class content {
private:
	int alloc; //Allocation of class
	int NumOfContent;
	int **MinMaxDuration;
	double **MinMaxvCPU; //vCPU number
	double **MinMaxMemory; //in GB
	double **MinMaxStorage; //in GB
	double **MinMaxNetwork; //in Gbps


public:
	content();
	content(const content & t);
	content(const char* name);
	content & operator=(const content & t);
	~content();

	int isalloc() const;
	int getNumOfContent() const;

	//Getters for the parameters of the VM
	//with the option of per User return (Type=1,2,...)
	int getDuration(int Type,int MinMax=0);
	double getvCPU(int Type,int MinMax=0);
	double getMemory(int Type,int MinMax=0);
	double getStorage(int Type,int MinMax=0);
	double getNetwork(int Type,int MinMax=0);

	int **gMinMaxDuration() const;
	double **gMinMaxvCPU() const;
	double **gMinMaxMemory() const;
	double **gMinMaxStorage() const;
	double **gMinMaxNetwork() const;

	//Print for debugging purposes
	void printInfo();
	
	
};


#endif
