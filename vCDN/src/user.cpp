/* Copyright 2019 The RECAP Simulation Framework Authors. All
Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#include <user.h>
#include <cstdio>
using namespace std;

user::user()
{
	alloc=0;
	DCMSN=NULL;
	Endpoint=-1;
	numOfTries=0;
}

user::user(double L_Duration,double L_vCPU,double L_Memory,double L_Storage,double L_Network,double L_Endpoint)
{
	alloc=1;
	DCMSN=new double[5];
	DCMSN[0]=L_Duration;
	DCMSN[1]=L_vCPU;
	DCMSN[2]=L_Memory;
	DCMSN[3]=L_Storage;
	DCMSN[4]=L_Network;
	Endpoint=L_Endpoint;
	numOfTries=0;
}

user::user(double L_Duration,double L_vCPU,double L_Memory,double L_Storage,double L_Network,double L_Endpoint,int L_numOfTries)
{
	alloc=1;
	DCMSN=new double[5];
	DCMSN[0]=L_Duration;
	DCMSN[1]=L_vCPU;
	DCMSN[2]=L_Memory;
	DCMSN[3]=L_Storage;
	DCMSN[4]=L_Network;
	Endpoint=L_Endpoint;
	numOfTries=L_numOfTries;
}

user::user(const user &t)
{
	if(t.alloc)
	{
		alloc=1;
		DCMSN=new double[5];
		DCMSN[0]=t.DCMSN[0];
		DCMSN[1]=t.DCMSN[1];
		DCMSN[2]=t.DCMSN[2];
		DCMSN[3]=t.DCMSN[3];
		DCMSN[4]=t.DCMSN[4];
		Endpoint=t.Endpoint;
		numOfTries=t.numOfTries;
	}
}

user & user::operator=(const user & t)
{
    	if (this!=&t)
    	{
	        if (alloc)
	       	{
			alloc=0;
			delete[] DCMSN;
			DCMSN=NULL;
			Endpoint=-1;
			numOfTries=0;
		}
		alloc=t.alloc;
		if(alloc)
		{
			DCMSN=new double[5];
			DCMSN[0]=t.DCMSN[0];
			DCMSN[1]=t.DCMSN[1];
			DCMSN[2]=t.DCMSN[2];
			DCMSN[3]=t.DCMSN[3];
			DCMSN[4]=t.DCMSN[4];
			Endpoint=t.Endpoint;
			numOfTries=t.numOfTries;
		}
	}
	return *this;
}

user::~user()
{
	if (alloc)
	{
		alloc=0;
		delete[] DCMSN;
		DCMSN=NULL;
		Endpoint=-1;
		numOfTries=0;
	}
}

int user::isalloc() const
{
	return alloc;
}

double user::modifyDuration(double amount)
{
	DCMSN[0]+=amount;
	return (DCMSN[0]);
}

double *user::gDCMSN() const
{
	return DCMSN;
}

int user::gEndpoint() const
{
	return Endpoint;
}

int user::gnumOfTries() const
{
	return numOfTries;
}
