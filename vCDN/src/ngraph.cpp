/* Copyright 2019 The RECAP Simulation Framework Authors. All
Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#include <iostream>
#include <ngraph.h>
#include <mmio.h>
#include <qsort.h>
#include <cstdio>
#include <cstddef>
using namespace std;

graph::graph() {
	rows=0;
	cols=0;
	nnz=0;
	nnzt=0;
	alloc=0;
	ix=NULL;
	jx=NULL;
	x=NULL;
	d=NULL;
	Spec=NULL; //DOWNLOAD SPEED IN NOT USET [1]
	MaxNodes=NULL;
	TypStr=NULL;
	OffsetLevel=NULL;
	nP=nullptr;
	P=nullptr;
	sid=nullptr;
	AllNodes=0;
}

//Read Graphs in Matrix Market format
graph::graph(const char* name)
{
    FILE *f; 
    MM_typecode matcode;
    int ret_code,M,N,nz,i;
    int *ti,*tj,temp,pat;
    double *tval;

    //Alloc should be initialized in case one of the following
    //tests fail and deconstructor kicks-in
    alloc=0;
    if ((f=fopen(name, "r")) == NULL)
    {
	cout<<"NGRAPH: file could not be opened - Terminating..."<<endl;
	exit(0);
    }
    if (mm_read_banner(f, &matcode) != 0)
    {
	cout<<"NGRAPH: Matrix Market banner could not be processed - Terminating..."<<endl;
	exit(0);
    }

    if (mm_is_complex(matcode) && mm_is_array(matcode) && mm_is_skew(matcode) && mm_is_symmetric(matcode) && mm_is_hermitian(matcode) && mm_is_integer(matcode))
    {
	cout<<"NGRAPH: Unsupported matrix type - Terminating..."<<endl;
	exit(0);
    }
    
    pat=mm_is_pattern(matcode);
    
    if ((ret_code = mm_read_mtx_crd_size(f, &M, &N, &nz)) !=0)
    {
	cout<<"NGRAPH: Wrong crd size - Terminating..."<<endl;
    }
    
    rows=M;
    cols=N;
    Spec=NULL;
    MaxNodes=NULL;
    TypStr=NULL;
    nnz=nz;
    nnzt=nz;
    alloc=1;

    d=new double[rows];
    x=new double[nnz-rows];
    ix=new int[rows+1];
    jx=new int[nnz-rows];
  
    tval=new double[nnz];
    ti=new int[nnz];
    tj=new int[nnz];

    for(i=0;i<rows;i++)
	d[i]=0.0;
    for(i=0;i<rows+1;i++)
    {
        ix[i]=0;
    }
    temp=0; // Compiler happy instruction!!!
    for(i=0;i<nnz;i++)
    {
        if (pat==0)
            temp=fscanf(f,"%d %d %lg\n", &ti[i],&tj[i],&tval[i]);
        else
            temp=fscanf(f,"%d %d\n", &ti[i],&tj[i]);
        ti[i]--;
        tj[i]--;
    } 
    temp--; // Compiler happy instruction!!!!
    for(i=0;i<nnz;i++)
    {
	if(ti[i]!=tj[i])
	{	
	        ix[ti[i]+1]++;
	}
    }
    for(i=0;i<rows;i++)
    {
        ix[i+1]+=ix[i];
    }
    for(i=0;i<nnz;i++)
    {
	if (ti[i]==tj[i])
	{
		d[ti[i]]=tval[i];
	}
	else
	{
		jx[ix[ti[i]]]=tj[i];
    	        x[ix[ti[i]]]=tval[i];
		ix[ti[i]]++;
	}
    }
    for(i=rows;i>0;i--)
    {
        ix[i]=ix[i-1];
    }
    ix[0]=0;
    for(i=0;i<rows;i++)
    {
        Qsort(jx,x,ix[i],ix[i+1]-1,0);
    }

    int tmp=getMaxLevel();
    OffsetLevel=new int[tmp+1];
    OffsetLevel[0]=0;
    for(i=0;i<tmp;i++)
    {
		int num=0;
		for(int j=0;j<rows;j++)
		{
			if(d[j]==i+1)
				num++;
		}
		OffsetLevel[i+1]=OffsetLevel[i]+num;
    }

    nnz-=rows;
    nnzt-=cols;
    delete[] ti;
    delete[] tj;
    delete[] tval;
    fclose(f);
}

graph::graph(const char* name,const char* SpecsTypes_filename)
{
    FILE *f; 
    MM_typecode matcode;
    int ret_code,M,N,nz,i;
    int *ti,*tj,temp,pat;
    double *tval;

    //Alloc should be initialized in case one of the following
    //tests fail and deconstructor kicks-in
    alloc=0;
    if ((f=fopen(name, "r")) == NULL)
    {
	cout<<"NGRAPH: file could not be opened - Terminating..."<<endl;
	exit(0);
    }
    if (mm_read_banner(f, &matcode) != 0)
    {
	cout<<"NGRAPH: Matrix Market banner could not be processed - Terminating..."<<endl;
	exit(0);
    }

    if (mm_is_complex(matcode) && mm_is_array(matcode) && mm_is_skew(matcode) && mm_is_symmetric(matcode) && mm_is_hermitian(matcode) && mm_is_integer(matcode))
    {
	cout<<"NGRAPH: Unsupported matrix type - Terminating..."<<endl;
	exit(0);
    }
    
    pat=mm_is_pattern(matcode);
    
    if ((ret_code = mm_read_mtx_crd_size(f, &M, &N, &nz)) !=0)
    {
	cout<<"NGRAPH: Wrong crd size - Terminating..."<<endl;
    }
    
    rows=M;
    cols=N;
    Spec=NULL;
    MaxNodes=NULL;
    TypStr=NULL;
    nnz=nz;
    nnzt=nz;
    alloc=1;

    d=new double[rows];
    x=new double[nnz-rows];
    ix=new int[rows+1];
    jx=new int[nnz-rows];
  
    tval=new double[nnz];
    ti=new int[nnz];
    tj=new int[nnz];

    for(i=0;i<rows;i++)
	d[i]=0.0;
    for(i=0;i<rows+1;i++)
    {
        ix[i]=0;
    }

    temp=0; // Compiler happy instruction!!!
    for(i=0;i<nnz;i++)
    {
        if (pat==0)
            temp=fscanf(f,"%d %d %lg\n", &ti[i],&tj[i],&tval[i]);
        else
            temp=fscanf(f,"%d %d\n", &ti[i],&tj[i]);
        ti[i]--;
        tj[i]--;
    } 
    temp--; // Compiler happy instruction!!!!

    for(i=0;i<nnz;i++)
    {
	if(ti[i]!=tj[i])
	{	
	        ix[ti[i]+1]++;
	}
    }
    for(i=0;i<rows;i++)
    {
        ix[i+1]+=ix[i];
    }

    for(i=0;i<nnz;i++)
    {
	if (ti[i]==tj[i])
	{
		d[ti[i]]=tval[i];
	}
	else
	{
		jx[ix[ti[i]]]=tj[i];
    	        x[ix[ti[i]]]=tval[i];
		ix[ti[i]]++;
	}
    }

    for(i=rows;i>0;i--)
    {
        ix[i]=ix[i-1];
    }
    ix[0]=0;
    for(i=0;i<rows;i++)
    {
        Qsort(jx,x,ix[i],ix[i+1]-1,0);
    }

    int tmp=getMaxLevel();
    OffsetLevel=new int[tmp+1];
    OffsetLevel[0]=0;
    for(i=0;i<tmp;i++)
    {
		int num=0;
		for(int j=0;j<rows;j++)
		{
			if(d[j]==i+1)
				num++;
		}
		OffsetLevel[i+1]=OffsetLevel[i]+num;
    }

    nnz-=rows;
    nnzt-=cols;
    delete[] ti;
    delete[] tj;
    delete[] tval;
    fclose(f);
    parseSpecsTypes(SpecsTypes_filename);
}

//Copy constructor
graph::graph(const graph & t)
{

	if(t.isalloc())
	{
		rows=t.getNumOfSites();
		cols=rows;
		nnz=t.getConnections();
		nnzt=nnz;

		ix=new int[rows+1];
		jx=new int[nnz];
		x=new double[nnz];

		d=new double[rows];
		
		for(int i=0;i<rows+1;i++)
			ix[i]=(t.gix())[i];
			
	
		for(int i=0;i<nnz;i++)
		{
			jx[i]=(t.gjx())[i];
			x[i]=(t.gx())[i];
		}

		for(int i=0;i<rows;i++)
		{
			d[i]=(t.gd()[i]);
		}

		int tmp=t.getMaxLevel();
		OffsetLevel=new int[tmp+1];
		OffsetLevel[0]=0;
		for(int i=0;i<tmp;i++)
		{
			OffsetLevel[i+1]=t.getOffsetLevel(i+1);
		}
		AllNodes=t.AllNodes;
		if(AllNodes!=0)
		{
			
			Spec=new double*[AllNodes];
			MaxNodes=new int[rows];
			TypStr=new int*[AllNodes];
			P=new double*[AllNodes];
			nP=new int[AllNodes];
			sid=new int[AllNodes];
			for(int i=0;i<rows;i++)
			{
				MaxNodes[i]=t.MaxNodes[i];
			}
			for(int i=0;i<AllNodes;i++)
			{
				Spec[i]=new double[6];
				TypStr[i]=new int[1];
				nP[i]=t.nP[i];
				P[i]=new double[nP[i]];
				for(int j=0;j<nP[i];j++)
					P[i][j]=t.P[i][j];
				for(int j=0;j<6;j++)
					Spec[i][j]=t.Spec[i][j];
				for(int j=0;j<1;j++)
					TypStr[i][j]=t.TypStr[i][j];
				sid[i]=t.sid[i];
			}
		}
	}
}


//Get Upload Links of the "id" node
void graph::getULlinks(int id,int **ids,double **speeds,int *nids)
{
	int i,c=0,num=0,start,end;
	if (alloc)
	{
		(*nids)=0;
		(*ids)=NULL;
		(*speeds)=NULL;
		if (id<0 || id>=rows)
		{
			cout<<"NGRAPH: id should be in range"<<endl;
			exit(0);
		}
		start=ix[id];
		end=ix[id+1];
		for(i=start;i<end;i++)
		{
			if(d[id]<=d[jx[i]])
				num++;
		}

		(*nids)=num;
		if((*nids)>0)
		{
			(*ids)=new int[(*nids)];
			(*speeds)=new double[(*nids)];
			for(i=start;i<end;i++)
			{
				if(d[id]<=d[jx[i]])
				{
					(*ids)[c]=jx[i];
					(*speeds)[c]=x[i];
					c++;
				}
			}
		}
		(*nids)=c;
	}
}

//Get Download Links of the "id" node and their number
void graph::getDLlinks(int id,int **ids,double **speeds,int *nids)
{
	int i,c=0,num=0,start,end;
	if (alloc)
	{
		(*nids)=0;
		(*ids)=NULL;
		(*speeds)=NULL;
		if (id<0 || id>=rows)
		{
			cout<<"NGRAPH: id should be in range"<<endl;
			exit(0);
		}
		start=ix[id];
		end=ix[id+1];
		for(i=start;i<end;i++)
		{
			if(d[jx[i]]<=d[id])
				num++;
		}

		(*nids)=num;
		if((*nids)>0)
		{
			(*ids)=new int[(*nids)];
			(*speeds)=new double[(*nids)];
			for(i=start;i<end;i++)
			{
				if(d[jx[i]]<=d[id])
				{
					(*ids)[c]=jx[i];
					(*speeds)[c]=x[i];
					c++;
				}
			}
		}
		(*nids)=c;
	}
}

//Get Level of the "id" node and their number
int graph::getLevel(int id)
{
	if(alloc)
	{
		if (id<0 || id>=rows)
		{
			cout<<"NGRAPH: id should be in range"<<endl;
			exit(0);
		}
		return d[id];
	}
	else
		return 0;
}

int graph::getMaxLevel() const
{
	int mx=0;
	if(alloc)
	{
		for(int i=0;i<rows;i++)
			if(mx<d[i])
				mx=d[i];
		return mx;
	}
	return mx;
		
}

int graph::getOffsetLevel(int Type) const
{
	int c=0;
	if (alloc)
		c=OffsetLevel[Type];
	return c;
}

int graph::getMaxType()
{
	int mx=0;
	if(alloc && Spec!=NULL)
	{
		for(int i=0;i<rows;i++)
			if(mx<TypStr[i][0])
				mx=TypStr[i][0];
		return mx;
	}
	return mx;
		
}

//Get number of sites in the network
int graph::getNumOfSites() const
{
	return rows;	
}

//Get number of node of type id in the network
int graph::getNumOfSites(int id) const
{
	int num=0;
	if(alloc && id>0)
	{
		num=OffsetLevel[id]-OffsetLevel[id-1];
	}
	return num;
}

//Get number of connections in the network graph
int graph::getConnections() const
{
	return nnz;
}

void graph::parseSpecsTypes(const char* name)
{
	if(alloc)
	{
		FILE *f;
		int temp;
		if ((f=fopen(name, "r")) == NULL)
		{
			cout<<"NGRAPH: file could not be opened - Terminating..."<<endl;
			exit(0);
		}
		temp=fscanf(f,"%d\n",&AllNodes);
		Spec=new double*[AllNodes];
		MaxNodes=new int[rows];
		TypStr=new int*[AllNodes];
		sid=new int[AllNodes];
		nP=new int[AllNodes];
		P=new double*[AllNodes];
		for(int i=0;i<rows;i++)
			MaxNodes[i]=0;
		for(int i=0;i<AllNodes;i++)
		{	
			Spec[i]=new double[6];
			TypStr[i]=new int[1];
			temp=fscanf(f,"%d %lf %lf %lf %lf %lf %lf %d",&sid[i],&Spec[i][0],&Spec[i][1],&Spec[i][2],&Spec[i][3],&Spec[i][4],&Spec[i][5],&TypStr[i][0]);
			MaxNodes[sid[i]-1]++;
			
			if(TypStr[i][0]==0)
			{
				nP[i]=1;
				P[i]=new double[1];
				P[i][0]=0.0;
				temp=fscanf(f,"\n",NULL);
			}
			else
			{
				temp=fscanf(f,"%d",&nP[i]);
				P[i]=new double[nP[i]];
				for(int j=0;j<nP[i];j++)
				{
					temp=fscanf(f,"%lf",&P[i][j]);
				}
				temp=fscanf(f,"\n",NULL);
			}
		}
		temp--; //Compiler happy instruction
		fclose(f);
	}
}


double graph::getvCPU(int id)
{
	if(alloc && AllNodes!=0)
	{
		return Spec[id][0];
	}
	return 0.0;
}

double graph::getMemory(int id)
{
	if(alloc && AllNodes!=0)
	{
		return Spec[id][1];
	}
	return 0.0;
}

double graph::getStorage(int id)
{
	if(alloc && AllNodes!=0)
	{
		return Spec[id][2];
	}
	return 0.0;
}

double graph::getAvailability(int id)
{
	if(alloc && AllNodes!=0)
	{
		return Spec[id][3];
	}
}

// minmax: 0 gives min, 1 gives max
double graph::getMeanTime(int id,int minmax)
{
	if(alloc && AllNodes!=0)
	{
		return Spec[id][4+minmax];
	}
}

int graph::getMaxNodes(int id)
{
	if(alloc && AllNodes!=0)
	{
		return MaxNodes[id];
	}
	return 0;
}

int graph::getModelType(int id)
{
	if(alloc && AllNodes!=0)
	{
		return TypStr[id][0];
	}
	return 0;
}

void graph::getNodes(int id, int **ids, int *nids)
{
	if(alloc && id>0)
	{
		(*ids)=NULL;
		(*nids)=getNumOfSites(id);
		if ((*nids)>0)
		{
			(*ids)=new int[(*nids)];
			int c=0;
			for(int i=0;i<rows;i++)
			{
				if(d[i]==id)
				{
					(*ids)[c]=i;
					c++;
				}
			}
			(*nids)=c;
		}			
	}
}

int graph::isalloc() const
{
	return alloc;
}

graph & graph::operator=(const graph & t)
{
    	if (this!=&t)
    	{
		if (alloc)
		{
			if(AllNodes!=0)
			{
				for(int i=0;i<AllNodes;i++)
				{
					delete[] Spec[i];
					delete[] TypStr[i];
					delete[] P[i];
				}
				AllNodes=0;
				delete[] P;
				delete[] nP;
				delete[] Spec;
				delete[] MaxNodes;
				delete[] sid;
				delete[] TypStr;
				Spec=NULL;
				MaxNodes=NULL;
				TypStr=NULL;
				P=nullptr;
				nP=nullptr;
				sid=nullptr;
			}
			rows=0;
			cols=0;
			nnz=0;
			nnzt=0;
			delete[] ix;
			delete[] jx;
			delete[] x;
			delete[] d;
			delete[] OffsetLevel;
			ix=NULL;
			jx=NULL;
			x=NULL;
			d=NULL;
			OffsetLevel=NULL;
			alloc=0;
		}
		alloc=t.isalloc();
		if(alloc)
		{
			rows=t.getNumOfSites();
			cols=rows;
			nnz=t.getConnections();
			nnzt=nnz;

			ix=new int[rows+1];
			jx=new int[nnz];
			x=new double[nnz];

			d=new double[rows];
		
			for(int i=0;i<rows+1;i++)
				ix[i]=(t.gix())[i];
	
			for(int i=0;i<nnz;i++)
			{
				jx[i]=(t.gjx())[i];
				x[i]=(t.gx())[i];
			}

			for(int i=0;i<rows;i++)
			{
				d[i]=(t.gd()[i]);
			}

			int tmp=t.getMaxLevel();
			OffsetLevel=new int[tmp+1];
			OffsetLevel[0]=0;
			for(int i=0;i<tmp;i++)
			{
				OffsetLevel[i+1]=t.getOffsetLevel(i+1);
			}

			AllNodes=t.AllNodes;
			if(AllNodes!=0)
			{
			
				Spec=new double*[AllNodes];
				MaxNodes=new int[rows];
				sid=new int[AllNodes];
				TypStr=new int*[AllNodes];
				P=new double*[AllNodes];
				nP=new int[AllNodes];
				for(int i=0;i<rows;i++)
				{
					MaxNodes[i]=t.MaxNodes[i];
				}
				for(int i=0;i<AllNodes;i++)
				{
					Spec[i]=new double[6];
					TypStr[i]=new int[1];
					nP[i]=t.nP[i];
					P[i]=new double[nP[i]];
					for(int j=0;j<nP[i];j++)
						P[i][j]=t.P[i][j];
					for(int j=0;j<6;j++)
						Spec[i][j]=t.Spec[i][j];
					for(int j=0;j<1;j++)
						TypStr[i][j]=t.TypStr[i][j];
					sid[i]=t.sid[i];
				}
			}
		}
	}
	return *this;
}

void graph::sanityCheck()
{
	int warn=0;
	cout<<"NGRAPH: Report of Sanity Check"<<endl;
	if(alloc)
	{
		if(Spec!=NULL)
		{
			int minN=d[0];
			int maxN=d[0];
			
			for(int i=1;i<rows;i++)
			{
				if(d[i]<minN)
					minN=d[i];
				if(d[i]>maxN)
					maxN=d[i];
				
			}
			if (minN<1)
			{
				cout<<"NGRAPH: Nodes with id lower than 1 should not exist"<<endl;
				warn++;
			}
			if (maxN==minN)
			{
				cout<<"NGRAPH: The hierarchy has only one level"<<endl;
				warn++;
			}

			int nids=0;
			int *ids=NULL;
			double *speeds=NULL;
			int flag2=0;
			for(int i=0;i<rows;i++)
			{
				getDLlinks(i,&ids,&speeds,&nids);
				if(nids==0 && d[i]!=1)
				{
					cout<<"NGRAPH: Node "<<i<<" should have DL links"<<endl;
					warn++;
				}
				if(nids>0 && d[i]==1)
				{
					cout<<"NGRAPH: Node "<<i<<" should not have DL links"<<endl;
					warn++;
				}
				if(nids>0)
				{
					flag2=0;
					for(int j=0;j<nids;j++)
					{
						if(d[ids[j]]>=d[i])
						{
							flag2=1;
						}
					}
					if(flag2==1)
					{
						cout<<"NGRAPH: Node "<<i<<" has DL links with larger or equal id which is prohibited"<<endl;
						warn++;
					}
					nids=0;
					delete[] ids;
					delete[] speeds;
					ids=NULL;
					speeds=NULL;
				}
				getULlinks(i,&ids,&speeds,&nids);

				if(nids>0 && d[i]==maxN)
				{
					cout<<"NGRAPH: Node "<<i<<" should not have UL links"<<endl;
					warn++;
				}
				if(nids==0 && d[i]!=maxN)
				{
					cout<<"NGRAPH: Node "<<i<<" should have UL links"<<endl;
					warn++;
				}
				if(nids>0)
				{
					flag2=0;
					for(int j=0;j<nids;j++)
					{
						if(d[ids[j]]<=d[i])
						{
							flag2=1;
						}
					}
					if(flag2==1)
					{
						cout<<"NGRAPH: Node "<<i<<" has UL links with lower or equal id which is prohibited"<<endl;
						warn++;
					}
					nids=0;
					delete[] ids;
					delete[] speeds;
					ids=NULL;
					speeds=NULL;
				}
			}
		}
		else
		{
			cout<<"NGRAPH: Upload/Download bandwidth is missing"<<endl;
			warn++;
		}
	}
	else
	{
		cout<<"NGRAPH: graph structure is not allocated"<<endl;
		warn++;
	}
	cout<<"NGRAPH: Report Complete with "<<warn<<" warnings"<<endl;
}

int graph::getNumOfPPoints(int id)
{
	if(alloc && AllNodes!=0)
	{
		return (nP[id]);
	}
	return 0;
}

double *graph::getPPoints(int id)
{
	if(alloc && AllNodes!=0)
	{
		return (P[id]);
	}
	return 0;
}

//Low level access
int *graph::gix() const
{
	return ix;
}


int *graph::gjx() const
{
	return jx;
}

double *graph::gx() const
{
	return x;
}

double *graph::gd() const
{
	return d;
}

double **graph::gSpec() const
{
	return Spec;
}

int **graph::gTypStr() const
{
	return TypStr;
}

int *graph::gMaxNodes() const
{
	return MaxNodes;
}

int graph::getAllNodes()
{
	return AllNodes;
}

int graph::getSID(int id)
{
	if(alloc && AllNodes!=0)
	{
		return sid[id];
	}
	return -1;
}

graph::~graph()
{
	if(alloc)
	{
		if(AllNodes!=0)
		{
			for(int i=0;i<AllNodes;i++)
			{
				delete[] Spec[i];
				delete[] TypStr[i];
				delete[] P[i];
			}
			AllNodes=0;
			delete[] P;
			delete[] nP;
			delete[] Spec;
			delete[] MaxNodes;
			delete[] sid;
			delete[] TypStr;
			Spec=NULL;
			MaxNodes=NULL;
			TypStr=NULL;
			P=nullptr;
			nP=nullptr;
			sid=nullptr;
		}
		rows=0;
		cols=0;
		nnz=0;
		nnzt=0;
		delete[] ix;
		delete[] jx;
		delete[] x;
		delete[] d;
		delete[] OffsetLevel;
		ix=NULL;
		jx=NULL;
		x=NULL;
		d=NULL;
		OffsetLevel=NULL;
		alloc=0;
	}
}
