/* Copyright 2019 The RECAP Simulation Framework Authors. All
Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#include <iostream>
#include <random>
#include <sim.h>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <string>
#include <fstream>
#include <list>
#include <vector>
#include <omp.h>
#include <vm.h>
#include <set>
#include <sys/stat.h> 
#include <sys/types.h>
#include <fstream>
#include <cstddef>
#include <array>
#define mmin(a, b) ((a) < (b)) ? (a) : (b)
#define mmax(a, b) ((a) > (b)) ? (a) : (b)
#define msan 106
using namespace std;

sim::sim()
{
	alloc=0;
	Time=NULL;
	minmaxReqPerSec=NULL;
	passthrough=-1;
	search=0;
	threads=1;
	numOfVMs=0;
	pathstrategy=0;
	distSize=0;
	S=NULL;
	G=NULL;
	C=NULL;

	out_dir="./";

	elapsedTime=0.0;

	ldg=0;
	map=NULL;
	last=-2;

	allReq=0;

	invMap=nullptr;
	dist=nullptr;
}

sim::sim(const char* simprop,const char* graphprop,const char* siteprop,const char* contprop,const char *vmpos,const char* out_dir_name)
{
	elapsedTime=omp_get_wtime();
	alloc=1;
	int temp,premise,reqPerSec;
	int Unids=0,Dnids=0;
	int *Uids=NULL,*Dids=NULL;
	double *BUids=NULL,*BDids=NULL;
	int siteID,numReqPerTstep,additionalReq;
	FILE *f;

	if ((f=fopen(simprop, "r")) == NULL)
	{
		cout<<"SIM: file could not be opened - Terminating..."<<endl;
		exit(0);
	}	
	Time=new double[3];
	minmaxReqPerSec=new double[2];
	temp=fscanf(f,"%lf %lf %lf\n",&Time[0],&Time[1],&Time[2]);
	temp=fscanf(f,"%lf %lf\n",&minmaxReqPerSec[0],&minmaxReqPerSec[1]);
	temp=fscanf(f,"%d\n",&passthrough);
	temp=fscanf(f,"%d\n",&pathstrategy);
	temp=fscanf(f,"%d\n",&search);
	temp=fscanf(f,"%d\n",&threads);

	if (getenv("OMP_NUM_THREADS")!=NULL)
		threads = atoi(getenv("OMP_NUM_THREADS"));

	out_dir=out_dir_name;
        S=new vector<site>[1];
        G=new graph[1];
	G[0]=graph(graphprop,siteprop);	
	C=new content[1];
	C[0]=content(contprop);

	for(int i=0;i<G->getNumOfSites();i++)
	{
		Unids=0;
		Uids=NULL;
		BUids=NULL;
		G->getULlinks(i,&Uids,&BUids,&Unids);

		Dnids=0;
		Dids=NULL;
		BDids=NULL;
		G->getDLlinks(i,&Dids,&BDids,&Dnids);

		S->push_back(site(i,G->getLevel(i),G->getvCPU(i),G->getMemory(i),G->getStorage(i),Unids,Dnids,Uids,Dids,BUids,BDids));		

		if(Unids>0)
		{
			delete[] Uids;
			delete[] BUids;
		}
		if(Dnids>0)
		{
			delete[] Dids;
			delete[] BDids;
		}
	}
	for(int i=0;i<G->getAllNodes();i++)
	{
		S[0][G->getSID(i)-1].addnode(G->getvCPU(i),G->getMemory(i),G->getStorage(i),G->getModelType(i),G->getNumOfPPoints(i),G->getPPoints(i),G->getAvailability(i),G->getMeanTime(i,0),G->getMeanTime(i,1));
	}

	loadICforVMs(vmpos);

	dist=nullptr;
	if((f=fopen("distribution.txt", "r")) != NULL) {
		temp = fscanf(f,"%d\n",&distSize);
		dist = new int*[distSize];
		for(int i=0; i<distSize; i++) dist[i] = new int[2];
		for(int i=0; i<distSize; i++) {
			temp = fscanf(f,"%d %d %d\n",&siteID,&premise,&reqPerSec);
			dist[i][0] = siteID;
			dist[i][1] = premise;
			dist[i][2] = reqPerSec;
			numReqPerTstep = premise/(int)Time[1];
			additionalReq = premise%(int)Time[1];
			S[0][siteID].setMinReq(numReqPerTstep);
			S[0][siteID].setMaxReq(additionalReq);
		}
		fclose(f);
		temp--;//Compiler Happy Instructions
	}else {
		distSize=0;
	}

	//Initialize Map structure
	ldg=0;
	last=-2;
	map=new int[G->getNumOfSites()];
	for(int i=0;i<G->getNumOfSites();i++)
		map[i]=-1;
	allReq=0;
	temp--;//Compiler Happy Instructions

	invMap=nullptr;
	if ((f=fopen("map.txt", "r")) != NULL)
	{
		invMap=new int[G->getNumOfSites()];
		for(int i=0;i<G->getNumOfSites();i++)
		{
			fscanf(f,"%d\n",&invMap[i]);
		}
		fclose(f);
	}
}

void sim::loadICforVMs(const char *vmpos)
{
	if (alloc)
	{
		int L_numOfVMs;
		int temp;
		FILE *f;
		if ((f=fopen(vmpos, "r")) == NULL)
		{
			cout<<"SIM: VM file could not be opened - Terminating..."<<endl;
			exit(0);
		}
		temp=fscanf(f,"%d\n",&L_numOfVMs);
		numOfVMs=L_numOfVMs;
		
		int j=0;
		while (j<L_numOfVMs)
		{
			int L_ID;
			temp=fscanf(f,"%d",&L_ID);
			L_ID--;
			int Mach_ID;
			temp=fscanf(f,"%d",&Mach_ID);
			Mach_ID--;
			int Ctype;
			temp=fscanf(f,"%d",&Ctype);
			double vCPU,Memory,Storage;
			double chForCacheHit,*mults;
			mults=new double[4];
			temp=fscanf(f,"%lf %lf %lf %lf %lf %lf %lf %lf\n",&vCPU,&Memory,&Storage,&chForCacheHit,&mults[0],&mults[1],&mults[2],&mults[3]);
			vm t=vm(Ctype,vCPU,Memory,Storage,chForCacheHit,mults);
			(S[0][L_ID].gNodes()[0][Mach_ID]).addVM(t.gvCPU()[1],t.gMemory()[1],t.gStorage()[1]);
			t.assignNode(Mach_ID+1);
			S[0][L_ID].enqueVM(t);
			j++;
			delete[] mults;
		}
		fclose(f);
		temp--;//Compiler Happy Instructions
	}
}


sim::sim(const sim & t)
{
    if(t.alloc)
    {
        alloc=t.alloc;
	Time=new double[3];
	for(int i=0;i<3;i++)
		Time[i]=t.Time[i];
	minmaxReqPerSec=new double[2];
	for(int i=0;i<2;i++)
		minmaxReqPerSec[i]=t.minmaxReqPerSec[i];
	passthrough=t.passthrough;
	pathstrategy=t.pathstrategy;
	search=t.search;
	threads=t.threads;
	numOfVMs=t.numOfVMs;
	distSize=t.distSize;
        S=new vector<site>[1];
	S[0]=t.S[0];
        G=new graph[1];
	G[0]=t.G[0];
	C=new content[1];
	C[0]=t.C[0];

	out_dir=t.out_dir;
	elapsedTime=t.elapsedTime;

	ldg=0;
	last=-2;
	map=new int[G->getNumOfSites()];
	for(int i=0;i<G->getNumOfSites();i++)
		map[i]=-1;
	allReq=t.allReq;
	
	dist=NULL;
	if(t.dist != NULL) {
		dist=new int*[distSize];
		for(int i=0; i<distSize; i++) dist[i] = new int[2];
		for(int i=0; i<distSize; i++) {
			dist[i][0]=t.dist[i][0];
			dist[i][1]=t.dist[i][1];
			dist[i][2]=t.dist[i][2];
		}
	}

	invMap=nullptr;
	if (t.invMap!=nullptr)
	{
		invMap=new int[G->getNumOfSites()];
		for(int i=0;i<G->getNumOfSites();i++)
			invMap[i]=t.invMap[i];
	}
    }
}

sim & sim::operator=(const sim & t)
{
    	if (this!=&t)
    	{
            if (alloc)
            {
                alloc=0;
		delete[] Time;
		Time=NULL;
		delete[] minmaxReqPerSec;
		minmaxReqPerSec=NULL;
		passthrough=-1;
		pathstrategy=0;
		search=0;
		threads=1;
		numOfVMs=0;
                S->clear();
                delete[] S;
                delete[] G;
		delete[] C;
		S=NULL;
		G=NULL;
		C=NULL;
		
		out_dir="./";
		elapsedTime=0.0;

		ldg=0;
		last=-2;
		delete[] map;
		map=NULL;

		if(dist != NULL) {
			for(int i=0; i<distSize; i++) delete[] dist[i];
			delete[] dist;
			dist=NULL;
			distSize=0;
		}
		allReq=0;
		if (invMap!=nullptr)
		{
			delete[] invMap;
			invMap=nullptr;
		}
            }
            alloc=t.alloc;
            if (alloc)
            {
		Time=new double[3];
		for(int i=0;i<3;i++)
			Time[i]=t.Time[i];
		minmaxReqPerSec=new double[2];
		for(int i=0;i<2;i++)
			minmaxReqPerSec[i]=t.minmaxReqPerSec[i];
		passthrough=t.passthrough;
		pathstrategy=t.pathstrategy;
		search=t.search;
		threads=t.threads;
		numOfVMs=t.numOfVMs;
		S=new vector<site>[1];
		S[0]=t.S[0];
		G=new graph[1];
		G[0]=t.G[0];
		C=new content[1];
		C[0]=t.C[0];

		out_dir=t.out_dir;
		elapsedTime=t.elapsedTime;

		ldg=0;
		last=-2;
		map=new int[G->getNumOfSites()];
		for(int i=0;i<G->getNumOfSites();i++)
			map[i]=-1;
		allReq=t.allReq;

		dist=NULL;
		if(t.dist != NULL) {
			distSize=t.distSize;
			dist=new int*[distSize];
			for(int i=0; i<distSize; i++) dist[i] = new int[2];
			for(int i=0; i<distSize; i++) {
				dist[i][0]=-1;
				dist[i][1]=-1;
				dist[i][2]=-1;
			}
		}

		invMap=nullptr;
		if (t.invMap!=nullptr)
		{
			invMap=new int[G->getNumOfSites()];
			for(int i=0;i<G->getNumOfSites();i++)
				invMap[i]=t.invMap[i];
		}
            }
        }
        return *this;
}

sim::~sim()
{
    if(alloc)
    {
        alloc=0;
	delete[] Time;
	Time=NULL;
	delete[] minmaxReqPerSec;
	minmaxReqPerSec=NULL;
	passthrough=-1;
	pathstrategy=0;
	search=0;
	threads=1;
	numOfVMs=0;
        S->clear();

        delete[] S;
        delete[] G;
	delete[] C;

	S=NULL;
	G=NULL;
	C=NULL;

	out_dir="./";
	elapsedTime=0.0;

	ldg=0;
	last=-2;
	delete[] map;
	map=NULL;

	if(dist!=NULL) {
		cout<<"NOT NULL!"<<endl;
		for(int i=0; i<distSize; i++) delete[] dist[i];
		delete[] dist;
		dist=NULL;
		distSize=0;
	}
	
	allReq=0;

	if (invMap!=nullptr)
	{
		delete[] invMap;
		invMap=nullptr;
	}
    }
}

void sim::formDeploymentPath(int Site,int Content,double Duration,double vCPU,double Memory,double Storage,double Network,list<vector<int>> *svr)
{
	int nin=1;
	int *in=NULL;
	int r=2;
	int endPoint=-1;
	if(alloc)
	{
		while(1)
		{
			//Using a Map to avoid circles of connections
			if(map[Site]!=-1)
				break;

			ldg++;			
			map[Site]=last;
			last=Site;			

			int vms=-1;
			r=S[0][Site].checkAvailability(Content,Duration,vCPU,Memory,Storage, Network,endPoint,&vms);

			svr->push_back(vector<int>(4,-1));
			list<vector<int>>::reverse_iterator rit=svr->rbegin();
			(*rit)[0]=Site;
			(*rit)[1]=vms;
			(*rit)[2]=r;
			(*rit)[3]=endPoint;		

			if(r==0 || r==3)
			{
				break;
			}
			
			nin=S[0][Site].gNumOfUDLinks()[1];
			if(nin)
			{
				in=S[0][Site].gID_UDLinks()[1];
				int cached=-1;
				double uband=0.0;
				int iuband=-1;
				int gind=-1;
				//Search constituent site that has the content cached or if not 
				//search for the one that has the most free bandwidth
				for(int i=0;i<nin;i++)
				{
					//Using a Map to avoid circles of connections
					int cSite=in[i];
					if(map[cSite]!=-1)
						continue;

					int ind=S[0][cSite].getLocalLinkID(Site,0);
					if(ind==-1)
						cout<<Site<<" "<<cSite;
					double cBW=(S[0][cSite].gID_UDBWs()[0][ind]-S[0][cSite].gUL()[ind]);

					// Includes the existanse of a Cache in the search
					if(pathstrategy==0)
						if(S[0][cSite].isCached(Content))
						{
							cached=i;
							gind=ind;
							break;
						}
					if(cBW>=uband)
					{
						iuband=i;
						uband=cBW;
						gind=ind;
					}
				}
				if(cached>-1)
				{
					Site=in[cached];
					endPoint=gind;
				}
				else
				{
					Site=in[iuband];
					endPoint=gind;
				}
				nin=0;
				in=NULL;
			}
			else
				break;
		}
		for(int i=0;i<ldg;i++)
		{
			int t=map[last];
			map[last]=-1;
			last=t;
		}
		ldg=0;
	}
}

void sim::createDeployRequestList()
{
	int L_minSite=G->getOffsetLevel(G->getMaxLevel()-1);
	int L_maxSite=G->getOffsetLevel(G->getMaxLevel())-1;
	int **L_minmaxDur=C->gMinMaxDuration();
	double **L_minmaxvCPU=C->gMinMaxvCPU();
	double **L_minmaxMemory=C->gMinMaxMemory();
	double **L_minmaxStorage=C->gMinMaxStorage();
	double **L_minmaxNetwork=C->gMinMaxNetwork();
	int L_content=C->getNumOfContent();
	int num;

	random_device generator;
	uniform_real_distribution<double> distribution(0,1);

	if(dist!=NULL) {
		for(int i=0; i<distSize; i++) {
			if(dist[i][1]==S[0][dist[i][0]].gUsersAR()[0]) continue;

			for(int j=0; j<dist[i][2]; j++) {
				int cont = 1;
				int st=dist[i][0];
				double dur=L_minmaxDur[cont-1][0];

				double vC=L_minmaxvCPU[cont-1][0];
				double mem=L_minmaxMemory[cont-1][0];
				double sto=L_minmaxStorage[cont-1][0];
				double net=L_minmaxNetwork[cont-1][0];
				addUser(st,cont,dur,vC,mem,sto,net);
				allReq++;
			}
		}
	}
	else {
		for(int j=L_minSite;j<=L_maxSite;j++)
		{
			double r=distribution(generator);
			int num=(int)(minmaxReqPerSec[0]+((minmaxReqPerSec[1]-minmaxReqPerSec[0])*r));

			for(int i=0;i<num;i++)
			{
				r=distribution(generator);
				int cont = 1 + (int)(r*L_content);
				r=distribution(generator);
				int st=j;
				r=distribution(generator);
				double dur=L_minmaxDur[cont-1][0]+((L_minmaxDur[cont-1][1]-L_minmaxDur[cont-1][0])*r);

				r=distribution(generator);
				double vC=L_minmaxvCPU[cont-1][0]+((L_minmaxvCPU[cont-1][1]-L_minmaxvCPU[cont-1][0])*r);
				double mem=L_minmaxMemory[cont-1][0]+((L_minmaxMemory[cont-1][1]-L_minmaxMemory[cont-1][0])*r);
				double sto=L_minmaxStorage[cont-1][0]+((L_minmaxStorage[cont-1][1]-L_minmaxStorage[cont-1][0])*r);
				double net=L_minmaxNetwork[cont-1][0]+((L_minmaxNetwork[cont-1][1]-L_minmaxNetwork[cont-1][0])*r);
				addUser(st,cont,dur,vC,mem,sto,net);
			}
			allReq+=num;
		}
	}
}

void sim::addUser(int Site,int Content,double Duration,double vCPU,double Memory,double Storage,double Network)
{
	if(alloc)
	{
		list<vector<int>> svr;
		formDeploymentPath(Site,Content,Duration,vCPU,Memory,Storage,Network,&svr);
		list<vector<int>>::reverse_iterator it=svr.rbegin();
		if((*it)[2]==3)
		{
			int c=1;
			for(list<vector<int>>::iterator itt=svr.begin();itt!=svr.end();++itt,++c)
			{
				if(dist!=NULL) {
					if(S[0][(*itt)[0]].gType()==4 && S[0][(*itt)[0]].gUsersAR()[0]>=1 && !(S[0][(*itt)[0]].gUsersAR()[0]==dist[(*itt)[0]-msan][1]))
						S[0][(*itt)[0]].incLatUponReq(c,Network,(*itt)[3]);
					if(!(passthrough && (*itt)[2]==1) && !(S[0][(*itt)[0]].gType()==4 && S[0][(*itt)[0]].gUsersAR()[0]==dist[(*itt)[0]-msan][1]))
					{
						S[0][(*itt)[0]].addUserToVM(Duration,vCPU,Memory,Storage,Network,(*itt)[2],(*itt)[1],(*itt)[3]);
					}
				}else {
					if(S[0][(*itt)[0]].gType()==4 && S[0][(*itt)[0]].gUsersAR()[0]>=1) {
						S[0][(*itt)[0]].incLatUponReq(c,Network,(*itt)[3]);
					}
					if(!(passthrough && (*itt)[2]==1))
					{
						S[0][(*itt)[0]].addUserToVM(Duration,vCPU,Memory,Storage,Network,(*itt)[2],(*itt)[1],(*itt)[3]);
					}
				}

			}

		}
		else
		{
			list<vector<int>>::iterator tt=svr.begin();
			//Log rejection to the entrace point of the request
			S[0][(*tt)[0]].gUsersAR()[1]++;
			
		}
		svr.clear();
	}
}

int sim::isalloc() const
{
    return alloc;   
}

void sim::run()
{
	if(alloc)
	{
		ofstream file;
		mkdir(out_dir.c_str(),0777);
		file.open(out_dir+"/out.txt");
		for(double t=Time[0];t<=Time[1];t+=1.0)
		{
			createDeployRequestList();
			timestep(t);
		}
		elapsedTime=omp_get_wtime()-elapsedTime;
		file<<elapsedTime<<endl;
		file<<allReq<<endl;
		file.close();
	}
}

double *sim::gTime() const
{
    return Time;    
}

double *sim::gminmaxReqPerSec() const
{
    return minmaxReqPerSec;
}

vector<site> *sim::gS() const
{
    return S;
}

graph *sim::gG() const
{
    return G;
}

content *sim::gC() const
{
    return C;
}

int sim::getAllRequests()
{
	return allReq;
}

void sim::timestep(double tstep,double amount)
{
	int chunk=1;
	if(alloc)
	{
		vector<site>::iterator it;
		#pragma omp parallel for num_threads(threads) schedule(static,chunk)
		for(it=S->begin();it<S->end();it++)
			it->timestep(amount);
		toFile(tstep);

	}
}

void sim::toFile(double tstep)
{
	if(alloc)
	{
		if(fmod(tstep,Time[2])==0.0)
		{
			mkdir(out_dir.c_str(),0777);
			mkdir((out_dir+"/net").c_str(),0777);
			mkdir((out_dir+"/sites").c_str(),0777);
			string name=out_dir+"/net/out_net_";
			string name2=out_dir+"/sites/out_site_";
			name.append(to_string((int)tstep));
			name2.append(to_string((int)tstep));
			ofstream out(name);
			ofstream out2(name2);
			vector<site>::iterator it;

			//Export Sites
			int *active_links=new int[2];
			int *AT_nodes=new int[2];
			int prevUsers;
			double *UTvCPU=new double[3];
			double *UTMemory=new double[3];
			double *UTStorage=new double[3];

			if (invMap==nullptr)
			{
				for(it=S->begin();it<S->end();it++)
				{
					prevUsers=it->getPrevUsers();
					//Export Connections
					for(int j=0;j<it->gNumOfUDLinks()[0];j++)
						out<<it->gID()+1<<" "<<it->gID_UDLinks()[0][j]+1<<" "<<" "<<it->gUL()[j]<<" "<<it->gID_UDBWs()[0][j]<<endl;
					it->collect_stats(active_links,AT_nodes,UTvCPU,UTMemory,UTStorage);
					out2<<it->gID()+1<<" "<<it->gType()<<" "<<it->gCache_hm()[0]<<" "<<it->gCache_hm()[1]<<" "<<it->gUsersAR()[0]<<" "<<it->gUsersAR()[1]<<" "<<active_links[0]<<" "<<active_links[1]<<" "<<it->numOfVMs()<<" "<<AT_nodes[0]<<" "<<AT_nodes[1]<<" "<<UTvCPU[0]<<" "<<UTvCPU[1]<<" "<<UTvCPU[2]<<" "<<UTMemory[0]<<" "<<UTMemory[1]<<" "<<UTMemory[2]<<" "<<UTStorage[0]<<" "<<UTStorage[1]<<" "<<UTStorage[2]<<" "<<it->gpCons()<<" "<<it->gLatency()<<endl;
					if(search==1 && it->gType()!=1 && it->gUsersAR()[0]>0 && tstep!=Time[2]) {
						if(prevUsers==it->gUsersAR()[0])
							it->cleanup();
					}else
						it->setPrevUsers(it->gUsersAR()[0]);
				}
			}
			else
			{
				for(it=S->begin();it<S->end();it++)
				{
					prevUsers=it->getPrevUsers();
					//Export Connections
					for(int j=0;j<it->gNumOfUDLinks()[0];j++)
						out<<invMap[it->gID()]<<" "<<invMap[it->gID_UDLinks()[0][j]]<<" "<<" "<<it->gUL()[j]<<" "<<it->gID_UDBWs()[0][j]<<endl;
					it->collect_stats(active_links,AT_nodes,UTvCPU,UTMemory,UTStorage);
					out2<<invMap[it->gID()]<<" "<<it->gType()<<" "<<it->gCache_hm()[0]<<" "<<it->gCache_hm()[1]<<" "<<it->gUsersAR()[0]<<" "<<it->gUsersAR()[1]<<" "<<active_links[0]<<" "<<active_links[1]<<" "<<it->numOfVMs()<<" "<<AT_nodes[0]<<" "<<AT_nodes[1]<<" "<<UTvCPU[0]<<" "<<UTvCPU[1]<<" "<<UTvCPU[2]<<" "<<UTMemory[0]<<" "<<UTMemory[1]<<" "<<UTMemory[2]<<" "<<UTStorage[0]<<" "<<UTStorage[1]<<" "<<UTStorage[2]<<" "<<it->gpCons()<<" "<<it->gLatency()<<endl;
					if(search==1 && it->gType()!=1 && it->gUsersAR()[0]>0 && tstep!=Time[2]) {
						if(prevUsers==it->gUsersAR()[0])
							it->cleanup();
					}else
						it->setPrevUsers(it->gUsersAR()[0]);
				}
			}

			delete[] active_links;
			delete[] AT_nodes;
			delete[] UTvCPU;
			delete[] UTMemory;
			delete[] UTStorage;

			out.close();
			out2.close();
		}
	}
}


void sim::placements(double *pl)
{
	if (alloc)
	{
		vector<site>::iterator it;
		int *active_links=new int[2];
		int *AT_nodes=new int[2];
		double *UTvCPU=new double[3];
		double *UTMemory=new double[3];
		double *UTStorage=new double[3];

		for(it=S->begin();it<S->end();it++)
		{
			int ID=it->gID();
			pl[ID]=0.0;
			if(it->gVMs()->size()>0)
			{
				it->collect_stats(active_links,AT_nodes,UTvCPU,UTMemory,UTStorage);
//				pl[ID]=(UTvCPU[0]/UTvCPU[1]*(1.0/(1.0+exp(-5.0+10.0*numOfVMs/S->size()))))*(double)it->gVMs()->size();
//				pl[ID]=(1.0/(1.0+exp(5.0-10.0*UTvCPU[0]/UTvCPU[1])))*(double)it->gVMs()->size();
//				pl[ID]=(UTvCPU[0]/UTvCPU[1]>0.3)*(double)it->gVMs()->size();
				pl[ID]=(double)it->gVMs()->size();

			}
		}
		delete[] active_links;
		delete[] AT_nodes;
		delete[] UTvCPU;
		delete[] UTMemory;
		delete[] UTStorage;				

		
	}
}

