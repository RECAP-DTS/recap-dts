/* Copyright 2019 The RECAP Simulation Framework Authors. All
Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#include <iostream>
#include <site.h>
#include <cstdio>
#include <cstdlib>
#define mmin(a, b) ((a) < (b)) ? (a) : (b)
#define mmax(a, b) ((a) > (b)) ? (a) : (b)
#define RTT 0.06

using namespace std;

site::site()
{
	alloc=0;
	ID=-1;
	Type=0;
	UL=NULL;
	Cache_hm=NULL;
	NumOfUDLinks=NULL;
	ID_UDLinks=NULL;
	ID_UDBWs=NULL;
	CachedContent=NULL;
	UsersAR=NULL;
	VMs=NULL;
	Nodes=NULL;
	ucUsers=NULL;
	minReqPerTstep=0.0;
	maxReqPerTstep=0.0;
	prevUsers=0;

	pCons=0.0;
	Latency=0.0;
	premises=0;
}

site::site(int L_ID,int L_Type,double L_vCPU,double L_Memory,double L_Storage,int L_NumOfUL,int L_NumOfDL,int *L_IDOfUL,int *L_IDOfDL,double *L_BWOfUL,double *L_BWOfDL)
{
	alloc=1;
	ID=L_ID;
	Type=L_Type;
	Cache_hm=new int[2];
	Cache_hm[0]=0;
	Cache_hm[1]=0;
	NumOfUDLinks=new int[2];
	NumOfUDLinks[0]=L_NumOfUL;
	NumOfUDLinks[1]=L_NumOfDL;
	UsersAR=new int[2];
	UsersAR[0]=0;
	UsersAR[1]=0;
	
	ID_UDLinks=new int*[2];
	ID_UDBWs=new double*[2];
	if(L_NumOfUL>0)
	{
		ID_UDLinks[0]=new int[L_NumOfUL];
		ID_UDBWs[0]=new double[L_NumOfUL];
		UL=new double[L_NumOfUL];
	}
	for(int i=0;i<L_NumOfUL;i++)
	{
		ID_UDLinks[0][i]=L_IDOfUL[i];
		ID_UDBWs[0][i]=L_BWOfUL[i];
		UL[i]=0.0;
	}
	if(L_NumOfDL>0)
	{
		ID_UDLinks[1]=new int[L_NumOfDL];
		ID_UDBWs[1]=new double[L_NumOfDL];
	}
	for(int i=0;i<L_NumOfDL;i++)
	{
		ID_UDLinks[1][i]=L_IDOfDL[i];
		ID_UDBWs[1][i]=L_BWOfDL[i];
	}
	CachedContent=new map<int,int>[1];
	VMs=new list<vm>[1];
	ucUsers=new list<ucuser>[1];
	Nodes=new vector<node>[1];
	pCons=0.0;
	Latency=0.0;
	premises=0;
	prevUsers=0;
	minReqPerTstep=0.0;
	maxReqPerTstep=0.0;
}

void site::addnode(double L_vCPU,double L_Memory,double L_Storage,int L_ModelType,int L_nP,double *L_P,double L_availability,double L_meanTimeMin,double L_meanTimeMax)
{
	if(alloc)
	{
		Nodes->push_back(node(L_vCPU,L_Memory,L_Storage,L_ModelType,L_nP,L_P,L_availability,L_meanTimeMin,L_meanTimeMax));
	}
}

site::site(const site & t)
{

	if(t.isalloc())
	{
		alloc=t.alloc;
		ID=t.ID;
		Type=t.Type;
		Cache_hm=new int[2];
		VMs=new list<vm>[1];
		ucUsers=new list<ucuser>[1];
		CachedContent=new map<int,int>[1];
		Nodes=new vector<node>[1];

		for(int i=0;i<2;i++)
			Cache_hm[i]=t.Cache_hm[i];
		
		NumOfUDLinks=new int[2];
		NumOfUDLinks[0]=t.NumOfUDLinks[0];
		NumOfUDLinks[1]=t.NumOfUDLinks[1];
		UsersAR=new int[2];
		UsersAR[0]=t.UsersAR[0];
		UsersAR[1]=t.UsersAR[1];
		ID_UDLinks=new int*[2];
		ID_UDBWs=new double*[2];
		if(NumOfUDLinks[0]>0)
		{
			ID_UDLinks[0]=new int[NumOfUDLinks[0]];
			ID_UDBWs[0]=new double[NumOfUDLinks[0]];
			UL=new double[NumOfUDLinks[0]];
		}
		for(int i=0;i<NumOfUDLinks[0];i++)
		{
			ID_UDLinks[0][i]=t.ID_UDLinks[0][i];
			ID_UDBWs[0][i]=t.ID_UDBWs[0][i];
			UL[i]=t.UL[i];
		}
		if(NumOfUDLinks[1]>0)
		{
			ID_UDLinks[1]=new int[NumOfUDLinks[1]];
			ID_UDBWs[1]=new double[NumOfUDLinks[1]];
		}
		for(int i=0;i<NumOfUDLinks[1];i++)
		{
			ID_UDLinks[1][i]=t.ID_UDLinks[1][i];
			ID_UDBWs[1][i]=t.ID_UDBWs[1][i];
		}

		VMs[0]=t.VMs[0];
		CachedContent[0]=t.CachedContent[0];
		Nodes[0]=t.Nodes[0];
		ucUsers[0]=t.ucUsers[0];
		pCons=t.pCons;
		Latency=t.Latency;
		premises=t.premises;
		prevUsers=t.prevUsers;
		minReqPerTstep=t.minReqPerTstep;
		maxReqPerTstep=t.maxReqPerTstep;
	}
}

site & site::operator=(const site & t)
{

    	if (this!=&t)
    	{
		if(alloc)
		{
			alloc=0;
			Type=0;
			ID=-1;
			delete[] Cache_hm;
			Cache_hm=NULL;
			delete[] UsersAR;
			UsersAR=NULL;
			if(NumOfUDLinks[0]>0)
			{
				delete[] ID_UDLinks[0];
				delete[] ID_UDBWs[0];
				delete[] UL;
			}
			if(NumOfUDLinks[1]>0)
			{
				delete[] ID_UDLinks[1];
				delete[] ID_UDBWs[1];
			}
			delete[] ID_UDLinks;
			ID_UDLinks=NULL;
			UL=NULL;
			delete[] ID_UDBWs;
			ID_UDBWs=NULL;
			delete[] NumOfUDLinks;
			NumOfUDLinks=NULL;

			CachedContent->clear();
			delete[] CachedContent;
			CachedContent=NULL;
			VMs->clear();
			delete[] VMs;
			VMs=NULL;
			Nodes->clear();
			delete[] Nodes;
			Nodes=NULL;
			ucUsers->clear();
			delete[] ucUsers;
			ucUsers=NULL;
			pCons=0.0;
			Latency=0.0;
			premises=0;
			prevUsers=0;
			minReqPerTstep=0.0;
			maxReqPerTstep=0.0;

		}
		alloc=t.alloc;
		if(alloc)
		{
			ID=t.ID;
			Type=t.Type;
			Cache_hm=new int[2];
			Cache_hm[0]=t.Cache_hm[0];
			Cache_hm[1]=t.Cache_hm[1];
			UsersAR=new int[2];
			UsersAR[0]=t.UsersAR[0];
			UsersAR[0]=t.UsersAR[1];

			NumOfUDLinks=new int[2];
			NumOfUDLinks[0]=t.NumOfUDLinks[0];
			NumOfUDLinks[1]=t.NumOfUDLinks[1];
			ID_UDLinks=new int*[2];
			ID_UDBWs=new double*[2];
			if(NumOfUDLinks[0]>0)
			{
				ID_UDLinks[0]=new int[NumOfUDLinks[0]];
				ID_UDBWs[0]=new double[NumOfUDLinks[0]];
				UL=new double[NumOfUDLinks[0]];
			}
			for(int i=0;i<NumOfUDLinks[0];i++)
			{
				ID_UDLinks[0][i]=t.ID_UDLinks[0][i];
				ID_UDBWs[0][i]=t.ID_UDBWs[0][i];
				UL[i]=t.UL[i];
			}
			if(NumOfUDLinks[1]>0)
			{
				ID_UDLinks[1]=new int[NumOfUDLinks[1]];
				ID_UDBWs[1]=new double[NumOfUDLinks[1]];
			}
			for(int i=0;i<NumOfUDLinks[1];i++)
			{
				ID_UDLinks[1][i]=t.ID_UDLinks[1][i];
				ID_UDBWs[1][i]=t.ID_UDBWs[1][i];
			}
			CachedContent=new map<int,int>[1];
			VMs=new list<vm>[1];
			Nodes=new vector<node>[1];
			ucUsers=new list<ucuser>[1];

			for(int i=0;i<2;i++)
				Cache_hm[i]=t.Cache_hm[i];
			for(int i=0;i<2;i++)
				UL[i]=t.UL[i];

			CachedContent[0]=t.CachedContent[0];
			VMs[0]=t.VMs[0];
			Nodes[0]=t.Nodes[0];
			ucUsers[0]=t.ucUsers[0];
			pCons=t.pCons;
			Latency=t.Latency;
			premises=t.premises;
			minReqPerTstep=t.minReqPerTstep;
			maxReqPerTstep=t.maxReqPerTstep;
		}
	}
	return *this;
}

site::~site()
{
	if(alloc)
	{
		alloc=0;
		ID=-1;
		Type=0;
		delete[] Cache_hm;
		Cache_hm=NULL;
		delete[] UsersAR;
		UsersAR=NULL;
		if(NumOfUDLinks[0]>0)
		{
			delete[] ID_UDLinks[0];
			delete[] ID_UDBWs[0];
			delete[] UL;
		}
		if(NumOfUDLinks[1]>0)
		{
			delete[] ID_UDLinks[1];
			delete[] ID_UDBWs[1];
		}
		delete[] ID_UDLinks;
		ID_UDLinks=NULL;
		UL=NULL;
		delete[] ID_UDBWs;
		ID_UDBWs=NULL;
		delete[] NumOfUDLinks;
		NumOfUDLinks=NULL;
		CachedContent->clear();
		delete[] CachedContent;
		CachedContent=NULL;
		VMs->clear();
		delete[] VMs;
		VMs=NULL;
		Nodes->clear();
		delete[] Nodes;
		Nodes=NULL;
		ucUsers->clear();
		delete[] ucUsers;
		ucUsers=NULL;
		pCons=0.0;
		Latency=0.0;
		premises=0;
		prevUsers=0;
		minReqPerTstep=0.0;
		maxReqPerTstep=0.0;
	}
}

int site::numOfVMs() const
{
	if(alloc)
		return VMs->size();
	else
		return 0;
}

void site::enqueVM(const vm & t)
{
	if(alloc)
	{
		int tp=t.gType();
		if(!isCached(tp))
			CachedContent[0][tp]=1;
		else
			CachedContent[0][tp]++;
		VMs->push_back(t);
	}
}

void site::cleanup()
{
	if(alloc)
	{
		list<vm>::iterator it=VMs[0].begin();
		while(it!=VMs[0].end())
		{
			if((it->CurNumOfUsers())==0)
			{
				int tp=it->gType();
				if(isCached(tp)==1)
					CachedContent->erase(tp);
				else
					CachedContent[0][tp]--;
				
				Nodes[0][it->gNode()-1].removeVM(it->gvCPU()[1],it->gMemory()[1],it->gStorage()[1]);
				it=VMs[0].erase(it);
			}
			else
				++it;
		}
	}
}

//Returns 1 on success
int site::VMallocStrategy(vm & t)
{
	int c=0;
	if(alloc)
	{
		for(int i=0;i<(int)Nodes->size();i++)
		{
			int r=Nodes[0][i].checkAvailability(t.gvCPU()[1],t.gMemory()[1],t.gStorage()[1]);
			if(r)
			{
				Nodes[0][i].addVM(t.gvCPU()[1],t.gMemory()[1],t.gStorage()[1]);
				t.assignNode(i+1);
				enqueVM(t);
				c=1;
				break;
			}
		}
	}
	return c;
}

double site::getLinkSpeed(int SiteID,int UpDown)
{
	double resp=-1.0;
	if(alloc)
	{
		for(int i=0;i<NumOfUDLinks[UpDown];i++)
		{
			if(ID_UDLinks[UpDown][i]==SiteID)
			{
				resp=ID_UDBWs[UpDown][i];
				break;
			}
		}
	}
	return resp;
}

int site::getLocalLinkID(int SiteID,int UpDown)
{
	int resp=-1;
	if(alloc)
	{
		for(int i=0;i<NumOfUDLinks[UpDown];i++)
		{
			if(ID_UDLinks[UpDown][i]==SiteID)
			{
				resp=i;
				break;
			}
		}
	}
	return resp;
}

//0 - Not deployed / 1 - Forwarded / 2 - Not Cached Deployed / 3 - Cached Deployed
int site::checkAvailability(int Content,double Duration,double L_vCPU,double L_Memory,double L_Storage,double L_Network,int endPoint,int *vmn)
{
	int c=0;
	if(alloc)
	{
		(*vmn)=-1;
		int deployed=isCached(Content);
		double remNet=0.0;
		int re = (endPoint==-1);
		if(!re)
			remNet=(ID_UDBWs[0][endPoint]-UL[endPoint]);

		//Caching is only applcable iff rand number <=ChangeForCacheHit
		if(deployed)
		{	
			int counter=0;
			for(list<vm>::iterator it=VMs->begin();it!=VMs->end();++it)
			{
				if((it->gType())==Content)
				{
					double ChanceForCacheHit=mmax(1.0*((double)Type==1),it->gchForCacheHit());
					double r_num=(((double)rand()) / RAND_MAX);
					int r = ((r_num<=ChanceForCacheHit));
					
					double mvCPU=(mmax(1.0,double(r)*it->gmults()[0]))*L_vCPU;
					double mMem=(mmax(1.0,double(r)*it->gmults()[1]))*L_Memory;
					double mSto=(mmax(1.0,double(r)*it->gmults()[2]))*L_Storage;
					double mNet=(mmax(1.0,double(r)*it->gmults()[3]))*L_Network;

					if(it->checkAvailability(mvCPU,mMem,mSto) && mNet*(!re)<=remNet && !(Nodes[0][it->gNode()-1].gup()))
					{
						c=2+r;
					
						(*vmn)=counter;
						break;
					}
					else
					{
						deployed--;
						if(!deployed)
							break;
					}
				}
				counter++;					
			}
		}
		if(!deployed && L_Network*(!(re))<=remNet)
		{
			c=1;
		}
	}
	return c;
}

//inc>=0 add to inc-th vm (cached deployment) else (uncached deployment) or (forwarding)
void site::addUserToVM(double Duration,double L_vCPU,double L_Memory,double L_Storage,double L_Network,int DeplType,int inc,int EndPoint)
{
	if(alloc)
	{

		if(DeplType==2)
		{

			int c=0;
			list<vm>::iterator it=VMs->begin();
			for(;it!=VMs->end() && c<inc;++it)
				c++;
			it->addUser(Duration,it->gmults()[0]*L_vCPU,it->gmults()[1]*L_Memory,it->gmults()[2]*L_Storage,it->gmults()[3]*L_Network,EndPoint);
			Cache_hm[1]++;
			if(EndPoint>-1)
				UL[EndPoint]+=it->gmults()[3]*L_Network;
			Nodes[0][it->gNode()-1].addUser(it->gmults()[0]*L_vCPU,it->gmults()[1]*L_Memory,it->gmults()[2]*L_Storage);	
		}
		else if(DeplType==3)
		{
			int c=0;
			list<vm>::iterator it=VMs->begin();
			for(;it!=VMs->end() && c<inc;++it)
				c++;
			it->addUser(Duration,L_vCPU,L_Memory,L_Storage,L_Network,EndPoint);
			Cache_hm[0]++;
			if(EndPoint>-1)
				UL[EndPoint]+=L_Network;
			Nodes[0][it->gNode()-1].addUser(L_vCPU,L_Memory,L_Storage);
		}
		else
		{
			addUserUncached(L_Network,Duration,EndPoint);
			if(EndPoint>-1)
				UL[EndPoint]+=L_Network;
		}
		UsersAR[0]++;

	}
}

void site::addUserUncached(double Netload,double Duration,int EndPoint)
{
	ucUsers->push_back(ucuser(Netload,Duration,EndPoint));
}

int site::isalloc() const
{
	return alloc;
}

int site::isCached(int L_Type) const
{
	int c=0;
	if(L_Type>0 && alloc)
	{
		map<int,int>::iterator itm;
		itm=CachedContent->find(L_Type);			
		if(itm!=CachedContent->end())
			c=itm->second;
	}
	return c;
}

int site::NumOfNodes() const
{
	return Nodes->size();
}


int site::gID() const
{
	return ID;
}

int site::gType() const
{
	return Type;
}

double *site::gUL() const
{
	return UL;
}

int *site::gCache_hm() const
{
	return Cache_hm;
}

int *site::gUsersAR() const
{
	return UsersAR;
}

int *site::gNumOfUDLinks() const
{
	return NumOfUDLinks;
}

int **site::gID_UDLinks() const
{
	return ID_UDLinks;
}

double **site::gID_UDBWs() const
{
	return ID_UDBWs;
}

map<int,int> *site::gCachedContent() const
{
	return CachedContent;
}

list<vm> *site::gVMs() const
{
	return VMs;
}

vector<node> *site::gNodes() const
{
	return Nodes;
}

list<ucuser> *site::gucUsers() const
{
	return ucUsers;
}

double site::gpCons() const
{
	return pCons;
}

double site::gLatency() const
{
	return Latency;
}

// Getters for min/max requests per time step
int site::getMinReq() const {
	return minReqPerTstep;
}

int site::getMaxReq() const {
	return maxReqPerTstep;
}

int site::getPrevUsers() {
	return prevUsers;
}

void site::setPrevUsers(int users) {
	prevUsers=users;
}

void site::timestep(double amount)
{
	if(alloc)
	{
		list<ucuser>::iterator itt=ucUsers->begin();
		if (NumOfUDLinks[0]>0)
		{
			double *nl=new double[NumOfUDLinks[0]];
			for(int i=0;i<NumOfUDLinks[0];i++)
				nl[i]=0.0;
			
			while(itt!=ucUsers->end())
			{
				double t=itt->reduceDuration(amount);
				if(t<=0)
				{
					if(itt->gendpoint()>-1)
						UL[itt->gendpoint()]-=itt->gnetload();
					itt=ucUsers->erase(itt);
				}
				else
					++itt;
			}
			double *rsrcs=new double[3];
			for(list<vm>::iterator it=VMs->begin();it!=VMs->end();++it)
			{
				for(int i=0;i<3;i++)
					rsrcs[i]=0.0;
				int nU=it->timestep(rsrcs,nl,amount);
				Nodes[0][it->gNode()-1].removeUser(nU,rsrcs[0],rsrcs[1],rsrcs[2]);
			}
			for(int i=0;i<NumOfUDLinks[0];i++)
				UL[i]-=nl[i];
			delete[] rsrcs;
			delete[] nl;
		}
		else
		{
			while(itt!=ucUsers->end())
			{
				double t=itt->reduceDuration(amount);

				if(t<=0)
				{
					itt=ucUsers->erase(itt);
				}
				else
					++itt;
			}
			double *rsrcs=new double[3];
			for(list<vm>::iterator it=VMs->begin();it!=VMs->end();++it)
			{
				for(int i=0;i<3;i++)
					rsrcs[i]=0.0;
				int nU=it->timestep(rsrcs,amount);
				Nodes[0][it->gNode()-1].removeUser(nU,rsrcs[0],rsrcs[1],rsrcs[2]);
			}
			delete[] rsrcs;
		}
		for(vector<node>::iterator in=Nodes->begin();in!=Nodes->end();++in)
		{
			if(in->gactive())
			{
				pCons+=in->getPowerConsumption();
				in->checkFailure();
			}
			
		}
	}
}

void site::collect_stats(int *active_links,int *AT_nodes,double *UTvCPU, double *UTMemory,double *UTStorage)
{
	active_links[0]=0;active_links[1]=ucUsers->size();
	AT_nodes[0]=0;AT_nodes[1]=Nodes->size();
	UTvCPU[0]=0.0;UTvCPU[1]=0.0;UTvCPU[2]=0.0;
	UTMemory[0]=0.0;UTMemory[1]=0.0;UTMemory[2]=0.0;
	UTStorage[0]=0.0;UTStorage[1]=0.0;UTStorage[2]=0.0;
	for(vector<node>::iterator in=Nodes->begin();in!=Nodes->end();++in)
	{
		active_links[0]+=in->gActiveLinks();
		AT_nodes[0]+=in->gactive();
		UTvCPU[0]+=in->gvCPU()[2];UTvCPU[1]+=in->gvCPU()[0];UTvCPU[2]+=in->gvCPU()[1];
		UTMemory[0]+=in->gMemory()[2];UTMemory[1]+=in->gMemory()[0];UTMemory[2]+=in->gMemory()[1];
		UTStorage[0]+=in->gStorage()[2];UTStorage[1]+=in->gStorage()[0];UTStorage[2]+=in->gStorage()[1];
	}
}

// Increase site's latency upon request entry(entry point or forwarded).
void site::incLatUponReq(double hops, double netload, int endpoint) {
	if(endpoint != -1) {
		Latency += RTT*hops + (netload/(ID_UDBWs[0][endpoint]-UL[endpoint]))*1000;
	}
	else
		Latency += RTT*hops;
}

// Setters for min/max requests per time step
void site::setMinReq(int minreq) {
	minReqPerTstep = minreq;
}

void site::setMaxReq(int maxreq) {
	maxReqPerTstep = maxreq;
}

//Printers for debugging
//Printing Info
void site::printInfo()
{
	if(alloc)
	{
		cout<<"Site ID                           : "<<ID<<endl;
		cout<<"|-Type                            : "<<Type<<endl;
		cout<<"|-Utilized / Total UL Bandwidth   : "<<UL[0]<<" / "<<UL[1]<<endl;
		cout<<"|-Cache Hits / Misses             : "<<Cache_hm[0]<<" / "<<Cache_hm[1]<<endl;
		cout<<"|-Total vCPUs Per Node            : "<<(Nodes[0][0].gvCPU())[1]<<endl;
		cout<<"|-Total Memory Per Node           : "<<(Nodes[0][0].gMemory())[1]<<endl;
		cout<<"|-Total Storage Per Node          : "<<(Nodes[0][0].gStorage())[1]<<endl;
		cout<<"|-Maximum Number of Nodes         : "<<Nodes->size()<<endl;
		cout<<"|-Number of UL/DL Links           : "<<NumOfUDLinks[0]<<" / "<<NumOfUDLinks[1]<<endl;
		cout<<" |-UL Links                       : ";
		for(int i=0;i<NumOfUDLinks[0];i++)
			cout<<ID_UDLinks[0][i]<<" ";
		cout<<endl;
		cout<<" |-DL Links                       : ";
		for(int i=0;i<NumOfUDLinks[1];i++)
			cout<<ID_UDLinks[1][i]<<" ";
		cout<<endl;
	}
}
