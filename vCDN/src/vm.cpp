/* Copyright 2019 The RECAP Simulation Framework Authors. All
Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#include <vm.h>
#include <user.h>
#include <iostream>
#include <cstdio>
using namespace std;

vm::vm()
{
	alloc=0;
	Type=0;
	vCPU=NULL;
	Memory=NULL;
	Storage=NULL;
	Users=NULL;
	Node=0;
	chForCacheHit=0.0;
	mults=NULL;
}

//Type, Number of vCPUs, Memory, Storage, Users
vm::vm(int L_Type,double L_vCPU,double L_Memory,double L_Storage,double L_chForCacheHit,double *L_mults)
{
	alloc=1;
	Type=L_Type;
	vCPU=new double[2];
	vCPU[0]=0.0;
	vCPU[1]=L_vCPU;
	Memory=new double[2];
	Memory[0]=0.0;
	Memory[1]=L_Memory;
	Storage=new double[2];
	Storage[0]=0.0;
	Storage[1]=L_Storage;
	Users=new list<user>[1];
	Node=0;
	chForCacheHit=L_chForCacheHit;
	mults=new double[4];
	for(int i=0;i<4;i++)
		mults[i]=L_mults[i];
}

vm::vm(const vm & t)
{
	if (t.alloc)
	{
		alloc=t.alloc;
		Type=t.Type;
		vCPU=new double[2];
		Memory=new double[2];
		Storage=new double[2];
		Node=t.Node;
		Users=new list<user>[1];
		Users[0]=t.Users[0];
		for(int i=0;i<2;i++)
		{
			vCPU[i]=t.vCPU[i];
			Memory[i]=t.Memory[i];
			Storage[i]=t.Storage[i];
		}
		chForCacheHit=t.chForCacheHit;
		mults=new double[4];
		for(int i=0;i<4;i++)
			mults[i]=t.mults[i];
	}
}

vm & vm::operator=(const vm & t)
{
    	if (this!=&t)
    	{
		if(alloc)
		{
			alloc=0;
			Type=0;
			delete[] vCPU;
			delete[] Memory;
			delete[] Storage;
			vCPU=NULL;
			Memory=NULL;
			Storage=NULL;
			Users[0].clear();
			delete[] Users;
			Users=NULL;
			Node=0;
			chForCacheHit=0;
			delete[] mults;
			mults=NULL;
		}
		alloc=t.alloc;
		if(alloc)
		{
			Type=t.Type;
			vCPU=new double[2];
			Memory=new double[2];
			Storage=new double[2];
			Node=t.Node;
			Users=new list<user>[1];
			Users[0]=t.Users[0];
			for(int i=0;i<2;i++)
			{
				vCPU[i]=t.vCPU[i];
				Memory[i]=t.Memory[i];
				Storage[i]=t.Storage[i];
			}
			chForCacheHit=t.chForCacheHit;
			mults=new double[4];
			for(int i=0;i<4;i++)
				mults[i]=t.mults[i];
		}
	}
	return *this;
}

vm::~vm()
{
	if(alloc)
	{
		alloc=0;
		Type=0;
		delete[] vCPU;
		delete[] Memory;
		delete[] Storage;
		vCPU=NULL;
		Memory=NULL;
		Storage=NULL;
		Users[0].clear();
		delete[] Users;
		Users=NULL;
		Node=0;
		chForCacheHit=0;
		delete[] mults;
		mults=NULL;
	}
}

int vm::isalloc() const
{
	return alloc;
}

void vm::assignNode(int L_Node)
{
	if(alloc)
	{
		Node=L_Node;
	}
}

int vm::checkAvailability(double L_vCPU,double L_Memory,double L_Storage)
{
	int c=0;
	if(alloc)
	{
		if(L_vCPU<=(vCPU[1]-vCPU[0]) && L_Memory<=(Memory[1]-Memory[0]) && L_Storage<=(Storage[1]-Storage[0]))
				c=1;
		
	}
	return c;
}

int vm::CurNumOfUsers() const
{
	if(alloc)
	{
		return Users->size();
	}
	return 0;
}


//User ID={0,1,...,n-1}
void vm::modifyDuration(int User,double amount)
{
	if(alloc && User>-1 && User<(int)Users->size())
	{
		list<user>::iterator it;
		it=Users->begin();
		advance(it,User);
		it->modifyDuration(amount);
	}
}

void vm::addUser(double L_Duration,double L_vCPU,double L_Memory,double L_Storage,double L_Network,int L_Endpoint)
{
	if(alloc)
	{
		vCPU[0]+=L_vCPU;
		Memory[0]+=L_Memory;
		Storage[0]+=L_Storage;
		Users->push_back(user(L_Duration,L_vCPU,L_Memory,L_Storage,L_Network,L_Endpoint));
	}
}

int vm::gType() const
{
	return Type;
}

double *vm::gvCPU() const
{
	return vCPU;
}

double *vm::gMemory() const
{
	return Memory;
}

double *vm::gStorage() const
{
	return Storage;
}

int vm::gNode() const
{
	return Node;
}

std::list<user> *vm::gUsers() const
{
	return Users;
}

double vm::gchForCacheHit() const
{
	return chForCacheHit;
}

double *vm::gmults() const
{
	return mults;
}

int vm::timestep(double *reqAmounts,double amount)
{
	int numU=0;
	if(alloc)
	{	
		list<user>::iterator it=Users->begin();
		while(it!=Users->end())
		{
			int rd=it->modifyDuration(-amount);
			
			if(rd<=0.0)
			{
				vCPU[0]-=it->gDCMSN()[1];
				Memory[0]-=it->gDCMSN()[2];
				Storage[0]-=it->gDCMSN()[3];			
				reqAmounts[0]+=it->gDCMSN()[1];
				reqAmounts[1]+=it->gDCMSN()[2];
				reqAmounts[2]+=it->gDCMSN()[3];
				it=Users->erase(it);
				numU++;
			}
			else
				++it;
		}
	}
	return numU;
}

int vm::timestep(double *reqAmounts,double *nl,double amount)
{
	int numU=0;
	if(alloc)
	{	
		list<user>::iterator it=Users->begin();
		while(it!=Users->end())
		{
			int rd=it->modifyDuration(-amount);
			
			if(rd<=0.0)
			{
				vCPU[0]-=it->gDCMSN()[1];
				Memory[0]-=it->gDCMSN()[2];
				Storage[0]-=it->gDCMSN()[3];			
				reqAmounts[0]+=it->gDCMSN()[1];
				reqAmounts[1]+=it->gDCMSN()[2];
				reqAmounts[2]+=it->gDCMSN()[3];
				if(it->gEndpoint()>-1)
					nl[it->gEndpoint()]+=it->gDCMSN()[4];
				it=Users->erase(it);
				numU++;
			}
			else
				++it;
		}
	}
	return numU;
}
