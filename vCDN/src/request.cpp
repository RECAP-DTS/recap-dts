/* Copyright 2019 The RECAP Simulation Framework Authors. All
Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#include <request.h>
#include <cstdio>
using namespace std;

request::request()
{
	alloc=0;
	content=0;
	site=0;
	duration=0.0;
	vCPU=0.0;
	memory=0.0;
	storage=0.0;
	network=0.0;
}

request::request(int L_content, int L_site, double L_duration, double L_vCPU, double L_memory, double L_storage, double L_network)
{
	alloc=1;
	content=L_content;
	site=L_site;
	duration=L_duration;
	vCPU=L_vCPU;
	memory=L_memory;
	storage=L_storage;
	network=L_network;
}

request::request(const request &t)
{
	if(t.alloc)
	{
		alloc=1;
		content=t.content;
		site=t.site;
		duration=t.duration;
		vCPU=t.vCPU;
		memory=t.memory;
		storage=t.storage;
		network=t.network;
	}
}

request & request::operator=(const request & t)
{
    	if (this!=&t)
    	{
	        if (alloc)
	       	{
			alloc=0;
			content=0;
			site=0;
			duration=0.0;
			vCPU=0.0;
			memory=0.0;
			storage=0.0;
			network=0.0;
		}
		alloc=t.alloc;
		if(alloc)
		{
			content=t.content;
			site=t.site;
			duration=t.duration;
			vCPU=t.vCPU;
			memory=t.memory;
			storage=t.storage;
			network=t.network;
		}
	}
	return *this;
}

request::~request()
{
	if (alloc)
	{
		alloc=0;
		content=0;
		site=0;
		duration=0.0;
		vCPU=0.0;
		memory=0.0;
		storage=0.0;
		network=0.0;
	}
}

int request::gcontent() const
{
	return content;
}

int request::gsite() const
{
	return site;
}

double request::gduration() const
{
	return duration;
}

double request::gvCPU() const
{
	return vCPU;
}

double request::gmemory() const
{
	return memory;
}

double request::gstorage() const
{
	return storage;
}

double request::gnetwork() const
{
	return network;
}
