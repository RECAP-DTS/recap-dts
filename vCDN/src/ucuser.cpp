/* Copyright 2019 The RECAP Simulation Framework Authors. All
Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#include <ucuser.h>
#include <cstdio>
using namespace std;

ucuser::ucuser()
{
	alloc=0;
	netload=0.0;
	duration=0.0;
	endpoint=-1;
}

ucuser::ucuser(double L_netload, double L_duration,int L_endpoint)
{
	alloc=1;
	netload=L_netload;
	duration=L_duration;
	endpoint=L_endpoint;
}

ucuser::ucuser(const ucuser &t)
{
	if(t.alloc)
	{
		alloc=1;
		netload=t.netload;
		duration=t.duration;
		endpoint=t.endpoint;
	}
}

ucuser & ucuser::operator=(const ucuser & t)
{
    	if (this!=&t)
    	{
	        if (alloc)
	       	{
			alloc=0;
			netload=0.0;
			duration=0.0;
			endpoint=-1;
		}
		alloc=t.alloc;
		if(alloc)
		{
			netload=t.netload;
			duration=t.duration;
			endpoint=t.endpoint;
		}
	}
	return *this;
}

ucuser::~ucuser()
{
	if (alloc)
	{
		alloc=0;
		netload=0;
		duration=0.0;
		endpoint=-1;
	}
}

int ucuser::isalloc() const
{
	return alloc;
}

double ucuser::gnetload() const
{
	return netload;
}

double ucuser::gduration() const
{
	return duration;
}

int ucuser::gendpoint() const
{
	return endpoint;
}

double ucuser::reduceDuration(double amount)
{
	duration-=amount;
	return duration;
}
