/* Copyright 2019 The RECAP Simulation Framework Authors. All
Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#include <request.h>
#include <rce.h>
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <random>

using namespace std;



void requestCreationEngine(int distribution,int min_reqs,int max_reqs,int min_content,int max_content,int min_site,int max_site,int min_duration,int max_duration)
{
	int i,num_reqs,content,site,duration;
	double r;


	if (distribution==1)
	{
		//RANDOM (SIMPLE) CASE
		cout<<"RANDOM DISTRIBUTION"<<endl;

		//RANDOMLY GENERATING NUMBER OF TAKS
		r = ((double)rand()) / RAND_MAX;
		num_reqs=min_reqs+r*(max_reqs-min_reqs);
		cout<<"number of requests: "<<num_reqs<<endl;
		//cout<<"max r: "<<RAND_MAX<<endl;
		//cout<<"random r: "<<r<<endl;

		//CREATION OF REQUEST LIST
		request *reqs;
		reqs=new request[num_reqs];

		//INITIALIZE RANDOMLY REQUESTS' ARGUMENT VALUES AND CREATE REQUESTS
		for(i=0;i<num_reqs;i++)
		{
			//CONTENT
			r = ((double)rand()) / RAND_MAX;
			content=min_content+r*(max_content-min_content);
			//SITE
			r = ((double)rand()) / RAND_MAX;
			site=min_site+r*(max_site-min_site);
			//DURATION
			r = ((double)rand()) / RAND_MAX;
			duration=min_duration+r*(max_duration-min_duration);

			//CREATE REQUEST
			reqs[i]=request(content,site,duration);
			cout<<"task "<<i<<": content: "<<reqs[i].gcontent()<<", site: "<<reqs[i].gsite()<<", duration: "<<reqs[i].gduration()<<endl;
		}

	}
	else if (distribution==2)
	{

		//UNIFORM DISTRIBUTION CASE
		cout<<"UNIFORM DISTRIBUTION"<<endl;

		//CREATION OF GENERATOR AND DISTRIBUTION FUNCTION
		std::default_random_engine uniform_generator;
		std::uniform_real_distribution<double> uniform_distr(0,1);

		//RANDOMLY GENERATING NUMBER OF TAKS
		r=uniform_distr(uniform_generator);
		num_reqs=min_reqs+r*(max_reqs-min_reqs);
		cout<<"number of requests: "<<num_reqs<<endl;
		//cout<<"min uniform: "<<uniform_distr.min()<<endl;
		//cout<<"max uniform: "<<uniform_distr.max()<<endl;
		//cout<<"random r: "<<r<<endl;

		//CREATION OF REQUEST LIST
		request *reqs;
		reqs=new request[num_reqs];
		
		for(i=0;i<num_reqs;i++)
		{
			//GENERATE RANDOM VALUE FOR EACH VARIABLE

			//CONTENT
			r = uniform_distr(uniform_generator);
			content=min_content+r*(max_content-min_content);
			//SITE
			r = uniform_distr(uniform_generator);
			site=min_site+r*(max_site-min_site);
			//DURATION
			r = uniform_distr(uniform_generator);
			duration=min_duration+r*(max_duration-min_duration);

			//CREATE REQUEST
			reqs[i]=request(content,site,duration);
			cout<<"task "<<i<<": content: "<<reqs[i].gcontent()<<", site: "<<reqs[i].gsite()<<", duration: "<<reqs[i].gduration()<<endl;
		}
	}
	else if (distribution==3)
	{

		//EXPONENTIAL DISTRIBUTION CASE
		cout<<"EXPONENTIAL DISTRIBUTION"<<endl;

		//CREATION OF GENERATOR AND DISTRIBUTION FUNCTION
		std::default_random_engine exp_generator;
		std::exponential_distribution<double> exponential_distr(exp(1));

		//RANDOMLY GENERATING NUMBER OF TAKS
		r = exponential_distr(exp_generator);
		r=r-int(r);
		num_reqs=min_reqs+r*(max_reqs-min_reqs);
		cout<<"number of requests: "<<num_reqs<<endl;
		//cout<<"lambda exponential: "<<exponential_distr.lambda()<<endl;
		//cout<<"min exponential: "<<exponential_distr.min()<<endl;
		//cout<<"max exponential: "<<exponential_distr.max()<<endl;
		//cout<<"random r: "<<r<<endl;
		//cout<<"-------------------------------------- "<<endl;


		//CREATION OF REQUEST LIST
		request *reqs;
		reqs=new request[num_reqs];
		
		for(i=0;i<num_reqs;i++)
		{
			//GENERATE RANDOM VALUE FOR EACH VARIABLE

			//CONTENT
			r = exponential_distr(exp_generator);
			r=r-int(r);
			content=min_content+r*(max_content-min_content);
			//SITE
			r = exponential_distr(exp_generator);
			r=r-int(r);
			site=min_site+r*(max_site-min_site);
			//DURATION
			r = exponential_distr(exp_generator);
			r=r-int(r);
			duration=min_duration+r*(max_duration-min_duration);

			//CREATE REQUEST
			reqs[i]=request(content,site,duration);
			cout<<"task "<<i<<": content: "<<reqs[i].gcontent()<<", site: "<<reqs[i].gsite()<<", duration: "<<reqs[i].gduration()<<endl;
		}
	}
	else if (distribution==4)
	{

		//GAMMA DISTRIBUTION CASE
		cout<<"GAMMA DISTRIBUTION"<<endl;

		//CREATION OF GENERATOR AND DISTRIBUTION FUNCTION
		std::default_random_engine gamma_generator;
		std::gamma_distribution<double> gamma_distr(1,1);

		//RANDOMLY GENERATING NUMBER OF TAKS
		r=gamma_distr(gamma_generator);
		r=r-int(r);
		num_reqs=min_reqs+r*(max_reqs-min_reqs);
		cout<<"number of requests: "<<num_reqs<<endl;
		//cout<<"min gamma: "<<gamma_distr.min()<<endl;
		//cout<<"max gamma: "<<gamma_distr.max()<<endl;
		//cout<<"random r: "<<r<<endl;
		//cout<<"-------------------------------------- "<<endl;

		//CREATION OF REQUEST LIST
		request *reqs;
		reqs=new request[num_reqs];
		
		for(i=0;i<num_reqs;i++)
		{
			//GENERATE RANDOM VALUE FOR EACH VARIABLE

			//CONTENT
			r = gamma_distr(gamma_generator);
			r=r-int(r);
			content=min_content+r*(max_content-min_content);
			//SITE
			r = gamma_distr(gamma_generator);
			r=r-int(r);
			site=min_site+r*(max_site-min_site);
			//DURATION
			r = gamma_distr(gamma_generator);
			r=r-int(r);
			duration=min_duration+r*(max_duration-min_duration);

			//CREATE REQUEST
			reqs[i]=request(content,site,duration);
			cout<<"task "<<i<<": content: "<<reqs[i].gcontent()<<", site: "<<reqs[i].gsite()<<", duration: "<<reqs[i].gduration()<<endl;
		}
	}
	else if (distribution==5)
	{

		//WEIBULL DISTRIBUTION CASE
		cout<<"WEIBULL DISTRIBUTION"<<endl;

		//CREATION OF GENERATOR AND DISTRIBUTION FUNCTION
		std::default_random_engine weibull_generator;
		std::weibull_distribution<double> weibull_distr(1,1);

		//RANDOMLY GENERATING NUMBER OF TAKS
		r=weibull_distr(weibull_generator);
		r=r-int(r);
		num_reqs=min_reqs+r*(max_reqs-min_reqs);
		cout<<"number of requests: "<<num_reqs<<endl;
		//cout<<"min weibull: "<<weibull_distr.min()<<endl;
		//cout<<"max weibull: "<<weibull_distr.max()<<endl;
		//cout<<"random r: "<<r<<endl;
		//cout<<"-------------------------------------- "<<endl;


		//CREATION OF REQUEST LIST
		request *reqs;
		reqs=new request[num_reqs];
		
		for(i=0;i<num_reqs;i++)
		{
			//GENERATE RANDOM VALUE FOR EACH VARIABLE

			//CONTENT
			r = weibull_distr(weibull_generator);
			r=r-int(r);
			content=min_content+r*(max_content-min_content);
			//SITE
			r = weibull_distr(weibull_generator);
			r=r-int(r);
			site=min_site+r*(max_site-min_site);
			//DURATION
			r = weibull_distr(weibull_generator);
			r=r-int(r);
			duration=min_duration+r*(max_duration-min_duration);

			//CREATE REQUEST
			reqs[i]=request(content,site,duration);
			cout<<"task "<<i<<": content: "<<reqs[i].gcontent()<<", site: "<<reqs[i].gsite()<<", duration: "<<reqs[i].gduration()<<endl;
		}
	}


		
		//RANDOMLY SELECT SITE TO SEND THE TASK LIST


}







