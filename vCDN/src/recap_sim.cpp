/* Copyright 2019 The RECAP Simulation Framework Authors. All
Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#include <iostream>
#include <ngraph.h>
#include <sim.h>
#include <random>
#include <site.h>
#include <list>
#include <omp.h>
#include <iostream>

using namespace std;

// Series of input files
// 1) sim.txt (../input/sim.txt)
// 2) G.mtx (../input/G.mtx)
// 3) G.txt (../input/G.txt)
// 4) content.txt (../input/content.txt)
// 5) vm.txt (../input/vm.txt))
// 6) out directory

// out.txt (format)
// 1: success, 0: fail
// in case of success
// 2nd line has elapsed time
// 3rd line has number of all requests

int main(int argc, char **argv)
{
	if(argc==7)
	{
		sim S(argv[1],argv[2],argv[3],argv[4],argv[5],argv[6]);
		S.run();
	}
	return 0;
}


