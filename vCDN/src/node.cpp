/* Copyright 2019 The RECAP Simulation Framework Authors. All
Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#include <node.h>
#include <cstdio>
#include <cstddef>
#include <power.h>
#include <random>
using namespace std;

node::node()
{
	alloc=0;
	vCPU=NULL;
	Memory=NULL;
	Storage=NULL;
	numOfVMs=0;
	ActiveLinks=0;
	active=0;
	P=nullptr;
	
	availability=0.0;
	meanTimeMin=0.0;
	meanTimeMax=0.0;
	up=0;
	
}

int node::checkAvailability(double L_vCPU,double L_Memory,double L_Storage)
{
	int c=0;
	if(alloc)
	{
		if(L_vCPU<=(vCPU[1]-vCPU[0]) && L_Memory<=(Memory[1]-Memory[0]) && L_Storage<=(Storage[1]-Storage[0]))
			c=1;
	}
	return c;
}

//Should be used after check
void node::addVM(double L_vCPU,double L_Memory,double L_Storage)
{
	if(alloc)
	{
		active=1;
		vCPU[0]+=L_vCPU;
		Memory[0]+=L_Memory;
		Storage[0]+=L_Storage;
		numOfVMs++;
	}
}

void node::removeVM(double L_vCPU,double L_Memory,double L_Storage)
{
	if(alloc)
	{
		if(numOfVMs>1)
		{
			vCPU[0]-=L_vCPU;
			Memory[0]-=L_Memory;
			Storage[0]-=L_Storage;
			numOfVMs--;
		}
		else if (numOfVMs==1) //The case of 1 is retained for suppressing rounding errors
		{
			vCPU[0]=0.0;
			vCPU[2]=0.0;
			Memory[0]=0.0;
			Memory[2]=0.0;
			Storage[0]=0.0;
			Storage[2]=0.0;
			numOfVMs=0;
			ActiveLinks=0;
			active=0;
		}
	}
}

//Type, Number of vCPUs, Memory, Storage, Users
node::node(double L_vCPU,double L_Memory,double L_Storage,int L_ModelType,int L_nP,double *L_P,double L_availability,double L_meanTimeMin,double L_meanTimeMax)
{
	alloc=1;
	vCPU=new double[3];
	vCPU[0]=0.0;
	vCPU[1]=L_vCPU;
	vCPU[2]=0.0;
	Memory=new double[3];
	Memory[0]=0.0;
	Memory[1]=L_Memory;
	Memory[2]=0.0;
	Storage=new double[3];
	Storage[0]=0.0;
	Storage[1]=L_Storage;
	Storage[2]=0.0;
	numOfVMs=0;
	ActiveLinks=0;
	active=0;
	P=new power[1];
	P[0]=power(L_ModelType,L_nP,L_P);
	availability=L_availability;
	meanTimeMin=L_meanTimeMin;
	meanTimeMax=L_meanTimeMax;
	up=0;
}

node::node(const node & t)
{
	if (t.isalloc())
	{
		alloc=t.isalloc();
		vCPU=new double[3];
		vCPU[0]=t.gvCPU()[0];
		vCPU[1]=t.gvCPU()[1];
		vCPU[2]=t.gvCPU()[2];
		Memory=new double[3];
		Memory[0]=t.gMemory()[0];
		Memory[1]=t.gMemory()[1];
		Memory[2]=t.gMemory()[2];
		Storage=new double[3];
		Storage[0]=t.gStorage()[0];
		Storage[1]=t.gStorage()[1];
		Storage[2]=t.gStorage()[2];
		numOfVMs=t.gnumOfVMs();
		ActiveLinks=t.gActiveLinks();
		active=t.gactive();
		P=new power[1];
		P[0]=t.P[0];
		availability=t.availability;
		meanTimeMin=t.meanTimeMin;
		meanTimeMax=t.meanTimeMax;
		up=t.up;
	}
}

node & node::operator=(const node & t)
{
    	if (this!=&t)
    	{
		if(alloc)
		{
			alloc=0;
			delete[] vCPU;
			delete[] Memory;
			delete[] Storage;
			vCPU=NULL;
			Memory=NULL;
			Storage=NULL;
			numOfVMs=0;
			ActiveLinks=0;
			active=0;
			delete[] P;
			P=nullptr;
			availability=0.0;
			meanTimeMin=0.0;
			meanTimeMax=0.0;
			up=t.up;
		}
		alloc=t.isalloc();
		if(alloc)
		{
			vCPU=new double[3];
			vCPU[0]=t.gvCPU()[0];
			vCPU[1]=t.gvCPU()[1];
			vCPU[2]=t.gvCPU()[2];
			Memory=new double[3];
			Memory[0]=t.gMemory()[0];
			Memory[1]=t.gMemory()[1];
			Memory[2]=t.gMemory()[2];
			Storage=new double[3];
			Storage[0]=t.gStorage()[0];
			Storage[1]=t.gStorage()[1];
			Storage[2]=t.gStorage()[2];
			numOfVMs=t.gnumOfVMs();
			ActiveLinks=t.gActiveLinks();
			active=t.gactive();
			P=new power[1];
			P[0]=t.P[0];
			availability=t.availability;
			meanTimeMin=t.meanTimeMin;
			meanTimeMax=t.meanTimeMax;
			up=t.up;
		}
	}
	return *this;
}

node::~node()
{
	if(alloc)
	{
		alloc=0;
		delete[] vCPU;
		delete[] Memory;
		delete[] Storage;
		vCPU=NULL;
		Memory=NULL;
		Storage=NULL;
		numOfVMs=0;
		ActiveLinks=0;
		active=0;
		delete[] P;
		P=nullptr;
		availability=0.0;
		meanTimeMin=0;
		meanTimeMax=0;
		up=0;
	}
}

void node::addUser(double L_vCPU,double L_Memory,double L_Storage,int nUsers)
{
	if(alloc)
	{
		ActiveLinks+=nUsers;
		vCPU[2]+=L_vCPU;
		Memory[2]+=L_Memory;
		Storage[2]+=L_Storage;
	}
}

void node::addUser(double L_vCPU,double L_Memory,double L_Storage)
{
	if(alloc)
	{
		ActiveLinks++;
		vCPU[2]+=L_vCPU;
		Memory[2]+=L_Memory;
		Storage[2]+=L_Storage;
	}
}

void node::removeUser(double num,double L_vCPU,double L_Memory,double L_Storage)
{
	if(alloc)
	{
		ActiveLinks-=num;
		vCPU[2]-=L_vCPU;
		Memory[2]-=L_Memory;
		Storage[2]-=L_Storage;
	}
}

int node::isalloc() const
{
	return alloc;
}

double *node::gvCPU() const
{
	return vCPU;
}

double *node::gMemory() const
{
	return Memory;
}

double *node::gStorage() const
{
	return Storage;
}

int node::gnumOfVMs() const
{
	return numOfVMs;
}

int node::gActiveLinks() const
{
	return ActiveLinks;
}

int node::gactive() const
{
	return active;
}

int node::gup() const
{
	return up;
}

double node::getPowerConsumption()
{
	return(((double)(up==0))*P->getPowerConsumption(vCPU[2]/vCPU[1]));
}

void node::checkFailure()
{
	if(availability<100.0)
		if (up == 0)
		{
				random_device generator;
				uniform_real_distribution<double> distribution(0,1);
				double r1=distribution(generator);
				double r2=distribution(generator);
				up=(r1*100.0>availability)*(meanTimeMin+(meanTimeMax-meanTimeMin)*r2);
		}
		else
			up--;
}
