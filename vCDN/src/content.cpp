/* Copyright 2019 The RECAP Simulation Framework Authors. All
Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#include <content.h>
#include <iostream>
#include <cstdio>
using namespace std;

content::content()
{
	alloc=0;
	NumOfContent=0;
	MinMaxDuration=NULL;
	MinMaxvCPU=NULL;
	MinMaxMemory=NULL;
	MinMaxStorage=NULL;
	MinMaxNetwork=NULL;
}

content::content(const content & t)
{
	if (t.isalloc())
	{
		alloc=t.isalloc();
		NumOfContent=t.getNumOfContent();
		MinMaxDuration=new int*[NumOfContent];
		MinMaxvCPU=new double*[NumOfContent];
		MinMaxMemory=new double*[NumOfContent];
		MinMaxNetwork=new double*[NumOfContent];
		MinMaxStorage=new double*[NumOfContent];
		for(int i=0;i<NumOfContent;i++)
		{
			MinMaxDuration[i]=new int[2];
			MinMaxvCPU[i]=new double[2];
			MinMaxMemory[i]=new double[2];
			MinMaxNetwork[i]=new double[2];
			MinMaxStorage[i]=new double[2];
		}

		for(int i=0;i<NumOfContent;i++)
		{
			for(int j=0;j<2;j++)
			{
				MinMaxDuration[i][j]=t.gMinMaxDuration()[i][j];
				MinMaxvCPU[i][j]=t.gMinMaxvCPU()[i][j];
				MinMaxMemory[i][j]=t.gMinMaxMemory()[i][j];;
				MinMaxNetwork[i][j]=t.gMinMaxNetwork()[i][j];;
				MinMaxStorage[i][j]=t.gMinMaxStorage()[i][j];;
			}
		}
		
	}
}

content::content(const char* name)
{
	FILE *f; 
	int temp;
	//Alloc should be initialized in case one of the following
	//tests fail and deconstructor kicks-in
	alloc=0;
	if ((f=fopen(name, "r")) == NULL)
	{
		cout<<"NGRAPH: file could not be opened - Terminating..."<<endl;
		exit(0);
	}
	alloc=1;
	temp=fscanf(f,"%d\n",&NumOfContent);
	MinMaxDuration=new int*[NumOfContent];
	MinMaxvCPU=new double*[NumOfContent];
	MinMaxMemory=new double*[NumOfContent];
	MinMaxStorage=new double*[NumOfContent];
	MinMaxNetwork=new double*[NumOfContent];

	for(int i=0;i<NumOfContent;i++)
	{
		MinMaxDuration[i]=new int[2];
		MinMaxvCPU[i]=new double[2];
		MinMaxMemory[i]=new double[2];
		MinMaxStorage[i]=new double[2];
		MinMaxNetwork[i]=new double[2];
	}
	for(int i=0;i<NumOfContent;i++)
	{
		temp=fscanf(f,"%d %d %lf %lf %lf %lf %lf %lf %lf %lf\n",&MinMaxDuration[i][0],&MinMaxDuration[i][1],&MinMaxvCPU[i][0],&MinMaxvCPU[i][1],&MinMaxMemory[i][0],&MinMaxMemory[i][1],&MinMaxStorage[i][0],&MinMaxStorage[i][1],&MinMaxNetwork[i][0],&MinMaxNetwork[i][1]);

	}
	temp--;//Compiler Happy Instruction
}

content & content::operator=(const content & t)
{
    	if (this!=&t)
    	{
	        if (alloc)
        	{
			alloc=0;
			for(int i=0;i<NumOfContent;i++)
			{
				delete[] MinMaxDuration[i];
				delete[] MinMaxvCPU[i];
				delete[] MinMaxMemory[i];
				delete[] MinMaxStorage[i];
				delete[] MinMaxNetwork[i];
			}
			delete[] MinMaxDuration;
			delete[] MinMaxvCPU;
			delete[] MinMaxMemory;
			delete[] MinMaxStorage;	
			delete[] MinMaxNetwork;
			MinMaxDuration=NULL;
			MinMaxvCPU=NULL;
			MinMaxMemory=NULL;
			MinMaxStorage=NULL;
			MinMaxNetwork=NULL;
			NumOfContent=0;

        	}
        	alloc=t.isalloc();
        	if (alloc)
        	{
			NumOfContent=t.getNumOfContent();
			MinMaxDuration=new int*[NumOfContent];
			MinMaxvCPU=new double*[NumOfContent];
			MinMaxMemory=new double*[NumOfContent];
			MinMaxStorage=new double*[NumOfContent];
			MinMaxNetwork=new double*[NumOfContent];
			for(int i=0;i<NumOfContent;i++)
			{
				MinMaxDuration[i]=new int[2];
				MinMaxvCPU[i]=new double[2];
				MinMaxMemory[i]=new double[2];
				MinMaxStorage[i]=new double[2];
				MinMaxNetwork[i]=new double[2];
			}

			for(int i=0;i<NumOfContent;i++)
			{
				for(int j=0;j<2;j++)
				{
					MinMaxDuration[i][j]=t.gMinMaxDuration()[i][j];
					MinMaxvCPU[i][j]=t.gMinMaxvCPU()[i][j];
					MinMaxMemory[i][j]=t.gMinMaxMemory()[i][j];
					MinMaxStorage[i][j]=t.gMinMaxStorage()[i][j];
					MinMaxNetwork[i][j]=t.gMinMaxNetwork()[i][j];
				}
			}
        	}
        }
        return *this;	
}

content::~content()
{
	if(alloc)
	{
		alloc=0;
		for(int i=0;i<NumOfContent;i++)
		{
			delete[] MinMaxDuration[i];
			delete[] MinMaxvCPU[i];
			delete[] MinMaxMemory[i];
			delete[] MinMaxStorage[i];
			delete[] MinMaxNetwork[i];
		}
		delete[] MinMaxDuration;
		delete[] MinMaxvCPU;
		delete[] MinMaxMemory;
		delete[] MinMaxStorage;
		delete[] MinMaxNetwork;
		MinMaxDuration=NULL;
		MinMaxvCPU=NULL;
		MinMaxMemory=NULL;
		MinMaxStorage=NULL;
		MinMaxNetwork=NULL;
		NumOfContent=0;
	}
}

int content::isalloc() const
{
	return alloc;
}

int content::getNumOfContent() const
{
	return NumOfContent;
}

int content::getDuration(int Type,int MinMax)
{
	if(alloc)
	{
		return MinMaxDuration[Type-1][MinMax];
	}
	return 0;
}

double content::getvCPU(int Type,int MinMax)
{
	if(alloc)
	{
		return MinMaxvCPU[Type-1][MinMax];
	}
	return 0.0;
}

double content::getMemory(int Type,int MinMax)
{
	if(alloc)
	{
		return MinMaxMemory[Type-1][MinMax];	
	}
	return 0.0;
}

double content::getStorage(int Type,int MinMax)
{
	if(alloc)
	{
		return MinMaxStorage[Type-1][MinMax];
	}
	return 0.0;
}

double content::getNetwork(int Type,int MinMax)
{
	if(alloc)
	{
		return MinMaxNetwork[Type-1][MinMax];		
	}
	return 0.0;
}

int **content::gMinMaxDuration() const
{
	return MinMaxDuration;
}

double **content::gMinMaxvCPU() const
{
	return MinMaxvCPU;
}

double **content::gMinMaxMemory() const
{
	return MinMaxMemory;
}

double **content::gMinMaxStorage() const
{
	return MinMaxStorage;
}

double **content::gMinMaxNetwork() const
{
	return MinMaxNetwork;
}

//Print for debugging purposes
void content::printInfo()
{
	if(alloc)
	{
		cout<<"Number of Different Content : "<<NumOfContent<<endl;
		for(int i=0;i<NumOfContent;i++)
		{
			cout<<"|-ID                                        : "<<i+1<<endl;
			cout<<" |- Min/Max Duration per User               : "<<MinMaxDuration[i][0]<<" / "<<MinMaxDuration[i][1]<<endl;
			cout<<" |- Min/Max vCPU per VM per User            : "<<MinMaxvCPU[i][0]<<" / "<<MinMaxvCPU[i][1]<<endl;
			cout<<" |- Min/Max Memory per VM per User          : "<<MinMaxMemory[i][0]<<" / "<<MinMaxMemory[i][1]<<endl;
			cout<<" |- Min/Max Storage per VM per User         : "<<MinMaxStorage[i][0]<<" / "<<MinMaxStorage[i][1]<<endl;
			cout<<" |- Min/Max Network per VM per User         : "<<MinMaxNetwork[i][0]<<" / "<<MinMaxNetwork[i][1]<<endl;
			cout<<endl;
		}
	}
}
