/* Copyright 2019 The RECAP Simulation Framework Authors. All
Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#include <power.h>
#include <iostream>
#include <cstdio>
#include <cmath>
#include <cstddef>
#define mmin(a,b) (((a)<(b))?(a):(b))
#define mmax(a,b) (((a)>(b))?(a):(b))
using namespace std;

power::power()
{
	alloc=0;
	ModelType=0; // [...,-2,-1,1,2,...]
	nP=0;
	P=nullptr;
}

power::power(int L_ModelType,int L_nP,double *L_P)
{
	alloc=1;
	ModelType=L_ModelType;
	if(ModelType!=0)
	{
		nP=L_nP;
		P=new double[nP];
		for(int i=0;i<nP;i++)
			P[i]=L_P[i];
	}
	else
	{
		nP=0;
		P=nullptr;
	}
}

power::power(const power & t)
{
	if(t.alloc)
	{
	        alloc=t.alloc;
		ModelType=t.ModelType;
		nP=t.nP;
		if(nP>0)
		{
			P=new double[nP];
			for(int i=0;i<nP;i++)
				P[i]=t.P[i];
		}
	}
}

power & power::operator=(const power & t)
{
    	if (this!=&t)
    	{
            if (alloc)
            {
                alloc=0;
		ModelType=0;
		if(nP>0)
			delete[] P;
		nP=0;
		P=nullptr;
            }
	
            alloc=t.alloc;
            if (alloc)
            {
		ModelType=t.ModelType;
		nP=t.nP;
		if(nP>0)
		{
			P=new double[nP];
			for(int i=0;i<nP;i++)
				P[i]=t.P[i];
		}
            }
        }
        return *this;
}

power::~power()
{
    if(alloc)
    {
        alloc=0;
	ModelType=0;
	if (nP>0)
		delete[] P;
	nP=0;
	P=nullptr;
    }
}

int power::isalloc() const
{
	return alloc;
}

int power::gModelType() const
{
	return ModelType;
}

int power::gnP() const
{
	return nP;
}

double *power::gP() const
{
	return P;
}

//type>0
double power::getPowerConsumption(double u)
{
	double p=0.0;
	int a,b;
	switch (ModelType)
	{
		case -1:
			a=(int)(u/0.1);
			b=mmin(a+1,nP-1);
			p=(P[a]+(P[b]-P[a])*u)/(3600.0*1.0e6); //MWh			
			break;
		case 0:
			break;
		case 1:
			p=(P[0]+(P[1]-P[0])*u)/(3600.0*1.0e6); //MWh
			break;
		default:
			break;
	}
	return p;
}

void power::printInfo()
{
	if(alloc)
	{
		cout<<"Power Model           : "<<ModelType<<endl;
		cout<<"Number of Points      : "<<nP<<endl;
		cout<<"Power (W)             : ";
		for(int j=0;j<nP;j++)
			cout<<P[j]<<" ";
		cout<<endl;
	}
}
