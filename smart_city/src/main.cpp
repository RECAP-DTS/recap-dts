/* Copyright 2019 The RECAP Simulation Framework Authors. All
Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#include<iostream>
#include<vector>
#include<cmath>
#include <jsoncons/json.hpp>
#include <fstream>

using jsoncons::json;
using namespace std;

int main(int argc, char **argv)
{
	if(argc==3)
	{
	    int nx=3,ny=3;
	    int numOfEdge=7;
	    int numOfMid=3;
	    int numOfCloud=1;
	    int numOfRoutes=0;


	    ifstream rfile(argv[2]);
	    json jsonRoutes;
	    rfile>>jsonRoutes;

	    json& jsonRoute = jsonRoutes[0];

	    numOfRoutes = jsonRoutes["NumOfRoutes"].as<int>();
	    rfile>>numOfRoutes;

	    vector< vector<int> > routes(numOfRoutes, vector<int>(2));
	    vector<float> amounts(numOfRoutes);

	    for(int i=0;i<numOfRoutes;i++)
	    {
	    	jsonRoute = jsonRoutes["Routes"][i];
	    	routes[i][0] = jsonRoute["node1"].as<int>();
	    	routes[i][1] = jsonRoute["node2"].as<int>();
	    	amounts[i] = jsonRoute["amount"].as<int>();
	    }
	    rfile.close();

	    vector<float> Le(7);
	    ifstream lfile(argv[1]);
	    json jsonLoads;
	    lfile>>jsonLoads;

	    Le[0] = jsonLoads["load0"].as<int>();
	    Le[1] = jsonLoads["load1"].as<int>();
	    Le[2] = jsonLoads["load2"].as<int>();
	    Le[3] = jsonLoads["load3"].as<int>();
	    Le[4] = jsonLoads["load4"].as<int>();
	    Le[5] = jsonLoads["load5"].as<int>();
	    Le[6] = jsonLoads["load6"].as<int>();
	    lfile.close();

	    //for(int i=0; i<7; i++)
	    //	cout << Le[i] << endl;
/*	    lfile>>Le[0];
	    lfile>>Le[1];
	    lfile>>Le[2];
	    lfile>>Le[3];
	    lfile>>Le[4];
	    lfile>>Le[5];
	    lfile>>Le[6];
*/

	    vector< vector<int> > Ge(ny, vector<int>(nx));
	    Ge[0][0]=0;
	    Ge[0][1]=1;
	    Ge[0][2]=2;
	    Ge[1][0]=3;
	    Ge[1][1]=4;
	    Ge[1][2]=2;
	    Ge[2][0]=5;
	    Ge[2][1]=6;
	    Ge[2][2]=5;

	    vector< vector<int> > Gm(ny, vector<int>(nx));
	    Gm[0][0]=0;
	    Gm[0][1]=Gm[1][1]=Gm[1][0]=1;
	    Gm[2][0]=Gm[2][1]=Gm[2][2]=Gm[1][2]=Gm[0][2]=2;

//	    Gm[0][0]=Gm[1][0]=Gm[2][0]=0;
//	    Gm[0][1]=Gm[1][1]=Gm[2][1]=1;
//	    Gm[0][2]=Gm[1][2]=Gm[2][2]=2;

	    vector< vector<int> > Gc(ny, vector<int>(nx));
	    Gc[0][0]=Gc[1][0]=Gc[2][0]=0;
	    Gc[0][1]=Gc[1][1]=Gc[2][1]=0;
	    Gc[0][2]=Gc[1][2]=Gc[2][2]=0;

	    vector<int> Lm(numOfMid);
	    Lm[0]=Lm[1]=Lm[2]=0;

	    vector<int> offset(4);
	    offset[0]=0;
	    offset[1]=offset[0]+1;
	    offset[2]=offset[1]+3;
	    offset[3]=offset[2]+7;

	    vector< vector<int> > FinEdges(offset[3], vector<int>(offset[3]));
	    for(int i=0;i<offset[3];i++)
		for(int j=0;j<offset[3];j++)
			FinEdges[i][j]=0;    

	    for(int i=0;i<(int)amounts.size();i++)
	    {
		int ix1=floor(((float)routes[i][0])/ny);
		int jx1=routes[i][0]%nx;
		int ix2=floor(((float)routes[i][1])/ny);
		int jx2=routes[i][1]%ny;

		if(Gm[ix1][jx1]!=Gm[ix2][jx2])
		{
		    if(Lm[Gm[ix1][jx1]]>=Lm[Gm[ix2][jx2]])
		    {
		        FinEdges[offset[2]+Ge[ix1][jx1]][offset[1]+Gm[ix1][jx1]]=1;
		        FinEdges[offset[2]+Ge[ix2][jx2]][offset[1]+Gm[ix2][jx2]]=1;
		        FinEdges[offset[1]+Gm[ix1][jx1]][offset[0]+Gc[ix1][jx1]]=1;
		        FinEdges[offset[1]+Gm[ix2][jx2]][offset[0]+Gc[ix2][jx2]]=1;
		        Lm[Gm[ix2][jx2]] = Lm[Gm[ix2][jx2]]+amounts[i];
		    }
		    else
		    {
		        FinEdges[offset[2]+Ge[ix1][jx1]][offset[1]+Gm[ix1][jx1]]=1;
		        FinEdges[offset[2]+Ge[ix2][jx2]][offset[1]+Gm[ix2][jx2]]=1;
		        FinEdges[offset[1]+Gm[ix1][jx1]][offset[0]+Gc[ix1][jx1]]=1;
		        FinEdges[offset[1]+Gm[ix2][jx2]][offset[0]+Gc[ix2][jx2]]=1;
		        Lm[Gm[ix1][jx1]] = Lm[Gm[ix1][jx1]]+amounts[i];                  
		    }
		}
		else
		{
		    Lm[Gm[ix1][jx1]] = Lm[Gm[ix1][jx1]]+amounts[i];
		    FinEdges[offset[2]+Ge[ix1][jx1]][offset[1]+Gm[ix1][jx1]]=1;            
		    FinEdges[offset[2]+Ge[ix2][jx2]][offset[1]+Gm[ix2][jx2]]=1;          
		}
	    }

	    ofstream out("out.json");
	    json cons;
	    cons["Connections"] = json::array();
	    json nodes;

	    for(int i=0;i<offset[3];i++)
	    {
		for(int j=0;j<offset[3];j++)
		{
			if(FinEdges[i][j]>0) {
				nodes["parent"] = j;
				nodes["child"] = i;
				cons["Connections"].push_back(nodes);
			}
	    }
	    }
	    out<<pretty_print(cons)<<endl;
	    //cout << pretty_print(cons) << endl;
	    out.close();
	}
	return 0;
}
