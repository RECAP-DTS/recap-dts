# RECAP SMART_CITY Simulator

The RECAP Smart City Simulator is a generalized and extensible framework based on a simple heuristic algorithm, which targets to propose optimal connectivity between the nodes of a Fog/Edge topology, in the essence of QoS improvement. The framework is written using the C/C++ programming language.

The project provides a valuable optimal node connection proposal, in the essence of end-user latency minimisation, load balancing and optimal resource consumption and provisioning in the Fog/Edge infrastructure.

The architecture of the RECAP Smart City is comprised of a specific cloud infrastracture, divided in three layers. The first one, edge tier which is closest to the end users, is composed of 7 edge nodes. The second one, mid tier, is composed of 3 mid nodes and the third tier, the cloud one, is composed of 1 cloud node.

For feedback, troubleshooting and technical inquiries, please contact mspanopoulos@iti.gr or kgiannou@iti.gr

## Getting started

Clone the project:

     git clone https://RECAP-DTS@bitbucket.org/RECAP-DTS/recap-dts.git

## Prerequisites

* Required g++ version 4.7 or higher

## Input and Output Data

Input files are located under the *bin* folder of the Smart City project. Two input files should be provided to the framework, with specific order. The first one, namely *loads.json*, contains initial loads of the edge nodes. Structural, this file is composed of pairs, with a pair being the edge node ID and the respective initial load value. The exact format is, for example, *"load0": 15*, where *load0* denotes the load of the first edge node and *15* denotes the initial load value. Note that, loads input file has to be of this exact format. The second input file, namely *routes.json*, denotes all the user requested routes, from starting quadrant (corresponding node) to endpoint (corresponding node), along with the aggregated overhead to the respective route. The format for this file is the following. The first value, *NumOfRoutes* denotes the total number of routes to be processed and after that, routes are provided equal to the previous value. Each route, is composed of three additional values. Start point node *node1*, endpoint node *node2* and the corresponding overhead, denoted as *amount*. For example, *"node1": 1*, *"node2": 6*, *"amount": 20*. Again, the aforementioned format has to be followed. Finally, note that each file can be adjusted by the user before execution.

The output file, namely *out.json*, contains the proposed (optimal) *"Connections"* between the nodes. Note that, each node can only be connected to the immediate upper level. Additionally, each mid tier node would eventually connect to the cloud node, thus no calculations are required for the mid tier, as there is only one potential connection. The format is simple and the connections are denoted as pairs, for instance (*"child": 1"*, *"parent": 0*), where 0 denotes the cloud node and 1 the first mid tier node and in addition, the establishment of connection between those two.

## Project Build

Navigate to the directory of the project, open the terminal and enter

     $ make clean && make

## Run the RECAP Smart City Simulator

At the directory of the project, open the terminal and enter

     $ cd bin

where the executable is located, and then

     $ ./main loads.json routes.json

After running the experiment, the corresponding output file, *out.json*, is exported under the bin folder also. Note that the respective input files have to be provided in the aforementioned order and any adjustements have to be done before execution.

## Deliverables

The deliverables of the RECAP Project can be found at the following url:

     https://recap-project.eu/about/public-deliverables/

## Acknowledgements

RECAP was funded by the European Commission’s Horizon 2020 Programme for Research and Innovation under Grand Agreement No. 732667.
